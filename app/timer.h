#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>

typedef enum {
	tmrSysctl = 0U,
	tmrMain
} TimerType_t;

void SysTick_Handler(void);
void Timer_DelayMs(uint32_t n);
//void SysTick_DelayTicks(uint32_t n);

void Timer_Start(uint8_t timer);
uint32_t Timer_Elapsed(uint8_t timer);

#endif /* TIMER_H_ */
