#ifndef SYSCTL_H_
#define SYSCTL_H_

#include <stdint.h>

#define BOARD_LED_PORT 						0U

/* Gpio inputs */
#define PIN_IN_VDD_PGOOD 					11U
#define PIN_IN_3V3_PGOOD 					7U
#define PIN_IN_5V0_PGOOD 					9U
#define PIN_IN_DDR_PGOOD 					23U
#define PIN_IN_ATX_PWR_OK 					13U	// pgood out of P2, should assert after PS_ON driven to turn 12V atx on
#define PIN_IN_ATX_PWR						16U // 5v0 into U7, stat output would be high (3v3 ref)
#define PIN_IN_ASLEEP_3P3 					24U // graceful shutdown request from uP
#define PIN_IN_RESET_SW		 				kSYSCON_GpioPort0Pin20ToPintsel // ext button reset
#define PIN_IN_PWR_SW						kSYSCON_GpioPort0Pin21ToPintsel // ext button power
#define PIN_IN_RCW_SEL_SW 					18U	// dip switch selector for boot source
#define PIN_IN_RESET_REQ_IGNORE 			19U // dip switch selector to ignore reset sources
#define PIN_IN_RESET_REQ_B33 				30U // reset request from uP

/* Gpio outputs */
#define PIN_OUT_PS_ON 						22U
#define PIN_OUT_PORESET 					17U
#define PIN_OUT_CPUVDD_EN 					14U
#define PIN_OUT_LED_PLATFORM_FAIL 			25U
#define PIN_OUT_LED_GREEN_DRV				8U
#define PIN_OUT_RCW_SRC_SEL 				1U
#define PIN_OUT_SYS_POWER_DN                10U
#define PIN_OUT_10G_ENABLE                  26U

#define RESET_HOLDTIME_DEFAULT				3U
#define SYSCTL_DATA_STORE_IN_FLASH			0x00007f00

typedef enum {
	stateAtxPwrIdle = 1U,
	stateAtxReady,
	statePowerApplied,
	stateVddOn,
	stateActive,
	stateShutdown
} SysCtlPowerState_t;

typedef enum {
	bootSrcNand = 0U,
	bootSrcSdcard =  1U,
	bootSrcUnknown
} SysCtlBootSource_t;

typedef enum {
	atxPowerOn = 1U,
	atxPowerOff = 0U
} SysCtlAtxPowerState_t;

typedef enum {
    rstEnable = 5U,
    rstDisable = 8U
} SysCtlResetEnable_t;

typedef enum {
    inVddPgood = 0U,
    in3v3Pgood,
    in5v0Pgood,
    inDdrPgood,
    inRstB33,
    inIgnoreRst,
    inBootSel,
    inAsleep,
    inAtxPwr,
    inAtxPwrOk,
    inMax
} SysCtlInputIndex_t;

typedef enum {
    bsRelease = 0U,
    bsPress,
    bsLongActivated,
	bsShortActivated
} SysCtlButtonState_t;

typedef enum {
    lvlHigh = 1U,
    lvlLow = 0U
} SysCtlGpioLevel_t;

typedef enum {
	ioSet = 1U,
	ioClear = 2U,
	ioToggle = 3U,
	ioReset = 4U,
	ioUnknown = 5U
} SysCtlGpioAction_t;

typedef struct __attribute__((packed)) {
	uint8_t delaySecs;					/* Delay reset by seconds. Range is 0 to 10 */
} ApiSystemReset_t;

typedef struct __attribute__((packed)) {
	uint8_t nextBootSource;
} ApiSetNextBootSource_t;

typedef struct __attribute__((packed)) {
	uint8_t holdtimeSeconds;
} ApiSetResetHoldtime_t;

typedef struct __attribute__((packed)) {
	uint8_t enable;
} ApiSetResetEnabled_t;

typedef struct __attribute__((packed)) {
    uint8_t control;
} ApiSet10gEnable_t;

typedef struct __attribute__((packed)) {
	uint8_t atxPowerState;
	uint8_t currentBootSource;
	uint8_t nextBootSource;
	uint8_t switchBootSource;
	uint8_t resetEnabled;
	uint8_t resetHoldtime;
	uint8_t switchResetIgnore;
} ApiGetSystemState_t;

typedef struct __attribute__((packed)) {
	uint8_t resetEnable;
	uint8_t resetHoldtime;
} SysCtlDataStore_t;

typedef struct  {
	uint8_t rollavg;
	uint8_t pin;
	uint8_t value;
} SysCtlInput_t;

typedef struct {

    SysCtlPowerState_t currentState;                /* Power state of the system  */
	SysCtlBootSource_t currentBootSource;			/* Next boot source. See enum sio_boot_source_t */
	SysCtlBootSource_t nextBootSource;				/* Next boot source. See enum sio_boot_source_t */
	SysCtlBootSource_t nextBootSourceFromApi; 		/* Boot source defined by sw */

	uint8_t resetHoldtime;							/* Ext reset button hold time to trigger reset. Duration in second. Set by i2c API. Default is 5 seconds. Persisted in flash. */
	uint8_t resetEnabled;							/* Ext reset enable/disable. Set by i2c API. Default is enabled. Persisted in flash. */

	volatile uint8_t extResetState;					/* Flag to indicate press event. */
	volatile uint8_t extPowerState;					/* Flag to indicate ext panel power button press event. */
} SysCtlPrivateState_t;

typedef uint8_t(*TwiReinit_t)(void);

void SysCtl_Run(void);
void SysCtl_Init(TwiReinit_t twiReinitCallback);

/* API methods exposed over i2c */
uint8_t SysCtl_ApiGetSystemState(const uint8_t * recv, uint8_t * toSend);
uint8_t SysCtl_ApiSetResetHoldTime(const uint8_t * recv, uint8_t * toSend);
uint8_t SysCtl_ApiSetResetEnabled(const uint8_t * recv, uint8_t * toSend);
uint8_t SysCtl_ApiSetNextBootSource(const uint8_t * recv, uint8_t * toSend);
uint8_t SysCtl_ApiSetEnable10g(const uint8_t * recv, uint8_t * toSend);


#endif /* SYSCTL_H_ */
