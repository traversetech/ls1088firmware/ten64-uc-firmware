#ifndef API_H_
#define API_H_

#include <stdint.h>

#define API_DATA_MAX_SIZE                   128U
#define API_HEADER_PREAMB                   0xcabe
#define API_HEADER_SIZE                     4U
#define API_COMMANDS_MAX                    12U

typedef enum {
    apiBdInfoSetMac             = ((uint8_t)0x10),
    apiBdInfoGetInfo            = ((uint8_t)0x11),

    apiSysCtlGetState           = ((uint8_t)0x20),
    apiSysCtlSetResetHoldtime   = ((uint8_t)0x21),
    apiSysCtlSetResetEnable     = ((uint8_t)0x22),
    apiSysCtlSetNextBootsource  = ((uint8_t)0x23),
    apiSysCtlSet10gEnable       = ((uint8_t)0x24),

    apiFwupGetInfo              = ((uint8_t)0xA0),
    apiFwupInit                 = ((uint8_t)0xA1),
    apiFwupXfer                 = ((uint8_t)0xA2),
    apiFwupCheck                = ((uint8_t)0xA3),
    apiFwupBoot                 = ((uint8_t)0xA4),
} ApiCmd_t;

typedef struct {
    uint8_t cmd;
    uint8_t (*apiCall)(const uint8_t *, uint8_t *);
} ApiCall_t;

typedef struct __attribute__((packed)) {
    uint16_t preamb;
    uint8_t cmd;
    uint8_t len;
    uint8_t data[API_DATA_MAX_SIZE];
} ApiMessage_t;

void API_ProcessData(const uint8_t * rxData, uint8_t * txData, uint8_t * txLen);

#endif /* API_H_ */
