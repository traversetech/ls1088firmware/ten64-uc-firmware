# Ten64 System Microcontroller

The Ten64 board uses an [NXP LPC804](https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/general-purpose-mcus/lpc800-cortex-m0-plus-/low-cost-microcontrollers-mcus-based-on-arm-cortex-m0-plus-core:LPC80X)
microcontroller to control the power on and reset process, as well as acting as an EEPROM to hold the board
MAC address and certain settings, such as reset button hold time. 

An I2C endpoint is provided to interact with the microcontroller from the host.

The LPC804 flash is partitioned in such a way that firmware upgrades are possible (using a dual-slot / "A-B" layout).

There are corresponding utilities in U-Boot and Linux that can interact with the microcontroller.

## Compiling and Development
- [MCUXpresso](https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools-/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE) is the recommended IDE for development

    You can use the [LPC-Link2](https://www.nxp.com/design/microcontrollers-developer-resources/lpc-link2:OM13054) as a JTAG
    probe. The microcontroller JTAG header is J8 on Ten64 boards (underside of the board, not loaded on production boards)

- You can use an "arm-none-eabi" toolchain to compile together with CMake to compile from the command line.

Please note that the SlotA and SlotB images are different binaries due to the different linker configuration
for each slot.

## Credits
The initial prototype implementation of the Ten64 microcontroller was developed by 
Mathew McBride <matt@traverse.com.au> and used on revision-A boards only.

Vaughn Coetzee <vaughn@traverse.com.au> implemented the vast majority of the feature set, such as I2C, A-B updates and
online firmware upgrades.

The CMake toolchain definition is based on [cmake-arm-embedded](https://github.com/jobroe/cmake-arm-embedded).

Platform code (e.g most of what is not in `app`) is obtained from the [MCUXpresso SDK](https://mcuxpresso.nxp.com/en/welcome).