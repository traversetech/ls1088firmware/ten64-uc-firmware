
#include "CppUTest/TestHarness.h"

extern "C" {
#include "api.h"
#include "fwup.h"
#include "crc32.h"
#include "image.h"
#include "flash.h"
#include "fakes/fake_flash.h"
#include "timer.h"
#include "reset.h"
#include <stdlib.h>
#include <unistd.h>
}

TEST_GROUP(FwupTests){
    void setup() {
        Flash_Setup();
    }

    void teardown() {
        Flash_Teardown();
    }
};

void createFakeImageAndSend(const uint8_t bank_id,
                            const uint8_t imageType,
                            const FwupVersion_t version,
                            const char * sha,
                            const uint16_t length){
    ApiMessage_t recv;
    ApiMessage_t send;
    uint8_t txLength = 0;
    FwupRunState_t * runState;

    // header
    FwupImageHeader_t header;
    header.magic = FWUP_IMAGE_MAGIC;
    header.imageLength = length;
    header.imageType = imageType;
    header.versionMajor = version.maj;
    header.versionMinor = version.min;
    header.versionPatch = version.patch;
    strcpy(header.gitShortSHA, sha);
    memset(header.pad,0xFF,FWUP_HEADER_PAD_SIZE);

    uint32_t padLength;
    if ((header.imageLength)%FWUP_XFER_BLOCK_SIZE)
        padLength = header.imageLength+FWUP_XFER_BLOCK_SIZE;
    else
        padLength = header.imageLength;

    uint8_t * fwImg = (uint8_t *)malloc(padLength);
    memset(fwImg,0,padLength);

    // create random data
    for (int i=0; i<header.imageLength; i++)
        fwImg[i] = (uint8_t)rand();

    header.crc = crc32(fwImg, (uint32_t)header.imageLength);
    printf("Calc crc = %08X\n",header.crc);

    int blocks = padLength/FWUP_XFER_BLOCK_SIZE;
    printf("Start transfer of %d blocks\n",blocks);

    // init
    send.preamb = API_HEADER_PREAMB;
    send.cmd = apiFwupInit;
    send.len = 2;
    send.data[0] = bank_id;
    send.data[1] = blocks;

    API_ProcessData((uint8_t *)&send, (uint8_t *)&recv, &txLength);
    CHECK_EQUAL(txLength, API_HEADER_SIZE + sizeof(FwupRunState_t));
    CHECK_EQUAL(recv.preamb, API_HEADER_PREAMB);
    CHECK_EQUAL(recv.cmd, send.cmd);
    CHECK_EQUAL(recv.len, sizeof(FwupRunState_t));

    runState = (FwupRunState_t *)recv.data;
    CHECK_EQUAL(runState->state, stateInit);
    CHECK_EQUAL(runState->error, fwupSuccess);

    // transfer
    send.preamb = API_HEADER_PREAMB;
    send.cmd = apiFwupXfer;
    send.len = FWUP_XFER_BLOCK_SIZE;

    for (int block=0; block<blocks; block++){
        memcpy(send.data, &fwImg[block*FWUP_XFER_BLOCK_SIZE],FWUP_XFER_BLOCK_SIZE);
        API_ProcessData((uint8_t *)&send, (uint8_t *)&recv, &txLength);
        CHECK_EQUAL(txLength, API_HEADER_SIZE + sizeof(FwupTransferResult_t));
        CHECK_EQUAL(recv.preamb, API_HEADER_PREAMB);
        CHECK_EQUAL(recv.cmd, send.cmd);
        CHECK_EQUAL(recv.len, sizeof(FwupTransferResult_t));
        FwupTransferResult_t * result = (FwupTransferResult_t *)recv.data;
        //CHECK_EQUAL(result->addr, );
        //CHECK_EQUAL(result->currentBlock)
        CHECK_EQUAL(result->error, fwupSuccess);
    }

    // check
    send.preamb = API_HEADER_PREAMB;
    send.cmd = apiFwupCheck;
    send.len = sizeof(FwupImageHeader_t);
    memcpy(send.data, (uint8_t *)&header, sizeof(FwupImageHeader_t));

    API_ProcessData((uint8_t *)&send, (uint8_t *)&recv, &txLength);
    CHECK_EQUAL(txLength, API_HEADER_SIZE + sizeof(FwupRunState_t));
    CHECK_EQUAL(recv.preamb, API_HEADER_PREAMB);
    CHECK_EQUAL(recv.cmd, send.cmd);
    CHECK_EQUAL(recv.len, sizeof(FwupRunState_t));

    runState = (FwupRunState_t *)recv.data;
    CHECK_EQUAL(runState->state, stateCheck);
    CHECK_EQUAL(runState->error, fwupSuccess);

    free(fwImg);

    // boot
    send.preamb = API_HEADER_PREAMB;
    send.cmd = apiFwupBoot;
    send.len = 0;

    API_ProcessData((uint8_t *)&send, (uint8_t *)&recv, &txLength);
    CHECK_EQUAL(txLength, 0);
}

/*TEST(FwupTests, testFwupTransfer) {

    FwupVersion_t ver = {1,0,0};
    createFakeImageAndSend(bankId1, imgTypeStable, ver, "0000000\0", 7213);
}*/

TEST(FwupTests, testImageSetStable) {
    FwupImageInfo_t imgInfo;

    FwupVersion_t ver = {1,0,0};
    createFakeImageAndSend(bankId1, imgTypeStable, ver, "0000000\0", 7213);
    ver = {1,0,1};
    createFakeImageAndSend(bankId2, imgTypeCandidate, ver, "1111111\0", 6785);

    FwupBootImageData_t imgData;
    imgData.preamb = FWUP_IMAGE_MAGIC;
    imgData.bankId = bankId2;
    imgData.bootCount = 0;
    Flash_StoreData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));


    Image_SetAsStable();

    Fwup_ApiGetInfo(NULL, (uint8_t *)&imgInfo);

    // image in bank a should be old stable now
    CHECK_EQUAL(imgInfo.bankAImg.imageType, imgTypeOldStable);
    CHECK_EQUAL(imgInfo.bankAImg.versionMajor, 1);
    CHECK_EQUAL(imgInfo.bankAImg.versionMinor, 0);
    CHECK_EQUAL(imgInfo.bankAImg.versionPatch, 0);
    CHECK_EQUAL(imgInfo.bankAImg.imageLength, 7213);

    // image in bank b should be stable now
    CHECK_EQUAL(imgInfo.bankBImg.imageType, imgTypeStable);
    CHECK_EQUAL(imgInfo.bankBImg.versionMajor, 1);
    CHECK_EQUAL(imgInfo.bankBImg.versionMinor, 0);
    CHECK_EQUAL(imgInfo.bankBImg.versionPatch, 1);
    CHECK_EQUAL(imgInfo.bankBImg.imageLength, 6785);
}

TEST(FwupTests, testLoadVerify){

    FwupBootImageData_t imgData;
    FwupImageInfo_t imgInfo;

    FwupVersion_t ver = {1,0,0};
    createFakeImageAndSend(bankId1, imgTypeStable, ver, "0000000\0", 7213);
    ver = {1,0,1};
    createFakeImageAndSend(bankId2, imgTypeCandidate, ver, "1111111\0", 6785);

    Image_LoadVerifyRun();

    /*Flash_ReadData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    Fwup_ApiGetInfo(NULL, (uint8_t *)&imgInfo);

    // image in bank a should be old stable now
    CHECK_EQUAL(imgInfo.bankAImg.imageType, imgTypeOldStable);
    CHECK_EQUAL(imgInfo.bankAImg.versionMajor, 1);
    CHECK_EQUAL(imgInfo.bankAImg.versionMinor, 0);
    CHECK_EQUAL(imgInfo.bankAImg.versionPatch, 0);
    CHECK_EQUAL(imgInfo.bankAImg.imageLength, 7213);

    // image in bank b should be stable now
    CHECK_EQUAL(imgInfo.bankBImg.imageType, imgTypeStable);
    CHECK_EQUAL(imgInfo.bankBImg.versionMajor, 1);
    CHECK_EQUAL(imgInfo.bankBImg.versionMinor, 0);
    CHECK_EQUAL(imgInfo.bankBImg.versionPatch, 1);
    CHECK_EQUAL(imgInfo.bankBImg.imageLength, 6785);*/



}
