#include "timer.h"

volatile uint32_t delayCounter;
volatile uint32_t sysctlElapsed;
volatile uint32_t mainElapsed;

void SysTick_Handler(void)
{
    if (delayCounter != 0U)
    {
    	delayCounter--;
    }

    sysctlElapsed++;
    mainElapsed++;
}

void Timer_DelayMs(uint32_t n)
{
	delayCounter = n;
    while(delayCounter != 0U){}
}

void Timer_Start(uint8_t timer)
{
	if (tmrMain == timer)
	{
		mainElapsed = 0;
	}
	else if (tmrSysctl == timer)
	{
		sysctlElapsed = 0;
	}
}

uint32_t Timer_Elapsed(uint8_t timer)
{
	if (tmrMain == timer)
	{
		return mainElapsed;
	}
	else if (tmrSysctl == timer)
	{
		return sysctlElapsed;
	}
	else {
		return 0;
	}
}

