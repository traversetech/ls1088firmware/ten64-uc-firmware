#!/usr/bin/env python3

import subprocess
import binascii
import struct
import os
from git import Repo
import collections
import argparse

ImgVersion = collections.namedtuple('ImgVersion',
    'maj, min, patch, gitsha, branch')



image_slot = {
    "banka": 0x01,
    "bankb": 0x02}

IMG_IN_SYS_REL = 'in_sys_rel'
IMG_IN_APP_REL = 'in_app_rel'
IMG_TYPE_STABLE = 0x01
IMG_TYPE_CANDIDATE = 0x02

image_type_translate = {
    IMG_TYPE_STABLE: "stable",
    IMG_TYPE_CANDIDATE: "candidate"}

def run_process(proc=[]):
    p = subprocess.Popen(proc,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    return p.communicate()

def do_git_sha():
    p = subprocess.Popen(['/usr/bin/git', 'rev-parse', '--short', 'HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    o,e = p.communicate()
    return o.decode('utf-8').strip()

def build_header(fw_bin, img_v):

    ''' Header format:
        typedef struct __attribute__((packed)) {
        uint32_t magic;
        uint32_t crc;
        uint16_t imageLength;
        uint8_t imageType;
        uint8_t versionMajor;
        uint8_t versionMinor;
        uint8_t versionPatch;
        char gitShortSHA[8];
        uint8_t pad[42];
    '''

    HEADER_MAGIC = 0xBEAFCAFE
    data = None

    if not os.path.exists(fw_bin):
        raise Exception("Firmware binary {} does not exist".format(fw_bin))

    with open(fw_bin, "rb") as f:
        data = f.read()

    data_size = len(data)
    if (data_size == 0):
        raise Exception("Firmware binary {} is empty".format(fw_bin))

    if img_v.branch == IMG_IN_SYS_REL:
        img_type = IMG_TYPE_STABLE
    else:
        img_type = IMG_TYPE_CANDIDATE

    print("Version: {}.{}.{}".format(img_v.maj, img_v.min, img_v.patch))
    print("Type: {}".format(image_type_translate[img_type]))
    print("Length: {} B".format(data_size))

    crc32 = binascii.crc32(data) & 0xffffffff
    print("Crc32: {:08X}".format(crc32))

    print("Git sha: {}".format(git_sha))
    git_sha_null = git_sha + '\0'

    pad = []
    for i in range(42):
        pad.append(0xff)
    header = struct.pack("<LLHBBBB8s42B",
        HEADER_MAGIC,
        crc32,
        data_size,
        img_type,
        img_v.maj,
        img_v.min,
        img_v.patch,
        git_sha_null.encode('utf-8'),
        *pad)

    header_file = "image/header_{}.{}.{}_{}_{}.bin".format(
        img_v.maj,
        img_v.min,
        img_v.patch,
        git_sha,
        img_v.branch)

    with open(header_file, "w+b") as f:
        f.write(header)

    print("Header written to {}".format(header_file))
    return header_file

def build_sbl():
    os.chdir("build/sbl")
    ls = os.listdir()
    if 'build_release.sh' not in ls:
        return False

    o,e = run_process(['./build_release.sh'])
    #print("build release: e={}".format(e))

    ls = os.listdir('release')
    if 'lpc804sbl.bin' not in ls and 'lpc804sbl.elf' not in ls:
        return False

    os.chdir("../../")

    return True

def build_app(bank_id):
    if bank_id == 1:
        cd_path = 'build/app'
    else:
        cd_path = 'build/app2'

    os.chdir(cd_path)
    ls = os.listdir()
    if 'build_release.sh' not in ls:
        return False

    o,e = run_process(['./build_release.sh'])
    #print("build release: e={}".format(e))

    ls = os.listdir('release')
    if bank_id == 1:
        if 'lpcapp_banka.bin' not in ls:
            return False

    else:
        if 'lpcapp_bankb.bin' not in ls:
            return False

    os.chdir("../../")
    return True

def cat_image(sbl_file, header_file, app_file, output_file):

    with open(output_file, 'w+b') as f_out:
        if sbl_file:
            with open(sbl_file, 'r+b') as f_sbl:
                f_out.write(f_sbl.read())
        with open(app_file, 'r+b') as f_app:
            f_out.write(f_app.read())
        with open(header_file, 'r+b') as f_hdr:
            f_out.write(f_hdr.read())

def build_in_sys_rel_image(img_v):
    if not build_sbl():
        raise Exception("Failed to build sbl")

    if not build_app(1):
        raise Exception("Failed to build app")

    header_file = build_header('build/app/release/lpc804app.bin', img_v)
    sbl_file = "./build/sbl/release/lpcsbl.bin"
    app_file = "./build/app/release/lpcapp_banka.bin"
    output_file = "./image/lpcfw_{}.{}.{}_{}.img".format(
        img_v.maj,
        img_v.min,
        img_v.patch,
        img_v.gitsha)

    cat_image(sbl_file, header_file, app_file, output_file)

    # final step is too pad the rest of the image to 0x7F80
    pad = []
    for i in range(14208):
        pad.append(0xff)

    fill = struct.pack("<14208B", *pad)
    with open(output_file, "a+b") as f:
        f.write(fill)

def build_in_app_rel_images(img_v):
    if not build_app(1):
        raise Exception("Failed to build bank A app")

    header_file = build_header('build/app/release/lpc804app.bin', img_v)
    app_file = "./build/app/release/lpcapp_banka.bin"
    output_file = "./image/lpcfw_{}.{}.{}_{}_banka.img".format(
        img_v.maj,
        img_v.min,
        img_v.patch,
        img_v.gitsha)
    cat_image("", header_file, app_file, output_file)

    if not build_app(2):
        raise Exception("Failed to build bank B app")

    header_file = build_header('build/app2/release/lpc804app.bin', img_v)
    app_file = "./build/app2/release/lpcapp_bankb.bin"
    output_file = "./image/lpcfw_{}.{}.{}_{}_bankb.img".format(
        img_v.maj,
        img_v.min,
        img_v.patch,
        img_v.gitsha)
    cat_image("", header_file, app_file, output_file)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Build firmware image')
    parser.add_argument('--type',choices=[IMG_IN_SYS_REL,IMG_IN_APP_REL], default=IMG_IN_APP_REL)
    parser.add_argument('--version',help='Override version number (default: sourced from git tag)')
    args = parser.parse_args()

    # must be on release branch to build images
    build_type = args.type
    local_repo = Repo(path=".")

    git_sha = do_git_sha()
    if git_sha is None:
        raise Exception("Git sha error")

    # git version from the tag
    tags = local_repo.tags

    img_version = None
    version_string = None
    if (args.version != None):
        version_string = args.version
    else:
        for tag in tags:
            if tag.commit == local_repo.active_branch.commit:
                if tag.name.startswith('v') and tag.name.count('.') == 2:
                    version_string = tag.name
                    break

    if (version_string is None):
        raise Exception("Could not find a valid version string to use (from git tag or --version)")

    maj = int(version_string.split('.')[0][1:])
    min = int(version_string.split('.')[1])
    patch = int(version_string.split('.')[2])

    img_version = ImgVersion(maj=maj,min=min,patch=patch,gitsha=git_sha,branch=build_type)

    if img_version is None:
        raise Exception("Could not create a version object. No valid branch, git sha or version tag could be found.")

    if build_type == IMG_IN_SYS_REL:
        print("Building an ISP image for production flashing")
    else:
        print("Building an IAP image for DFU")

    print("Image version: v{}.{}.{}".format(img_version.maj, img_version.min, img_version.patch))
    print("Image git sha: {}".format(img_version.gitsha))

    if build_type == IMG_IN_SYS_REL:
        build_in_sys_rel_image(img_version)
    else:
        build_in_app_rel_images(img_version)
    #print(os.listdir("./image"))
