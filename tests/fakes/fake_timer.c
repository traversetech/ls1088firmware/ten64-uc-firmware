/*#include <chrono>
using namespace std::chrono;

auto begin = std::chrono::high_resolution_clock::now();

void SysTick_ElapsedStart(void){
    begin = std::chrono::high_resolution_clock::now();
}

uint32_t SysTick_ElapsedRead(void){
    auto end = high_resolution_clock::now();
    auto elapsed = duration_cast<std::chrono::microseconds>(end - begin);
    return elapsed.count();
}*/

#include <sys/time.h>
#include <stdint.h>
#include <stdio.h>

struct timeval begin;

void SysTick_ElapsedStart(void){
    gettimeofday(&begin, 0);
    //printf("Time start: %ld usec.\n", begin.tv_sec);
}

uint32_t SysTick_ElapsedRead(void){
    struct timeval end;
    gettimeofday(&end, 0);
    //long seconds = end.tv_sec - begin.tv_sec;
    long microseconds = end.tv_sec - begin.tv_sec;
    //printf("Time measured: now=%ld start=%ld, diff=%ld s\n", end.tv_sec, begin.tv_sec, microseconds);
    return (uint32_t)(microseconds*1000);
}
