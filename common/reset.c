#include "reset.h"
#include "LPC804.h"

void Lpc_Reset(){
    SCB->AIRCR = (0x5FA<<SCB_AIRCR_VECTKEY_Pos)| SCB_AIRCR_SYSRESETREQ_Msk;
}
