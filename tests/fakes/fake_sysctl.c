#include "fake_sysctl.h"
#include "sysctl.h"
#include "stdio.h"



typedef struct {
    uint8_t pin;
    uint8_t value;
} TestGpio_t;

TestGpio_t gpioInputs[11] = {
        {PIN_IN_VDD_PGOOD, 0U},
        {PIN_IN_3V3_PGOOD, 0U},
        {PIN_IN_5V0_PGOOD, 0U},
        {PIN_IN_DDR_PGOOD, 0U},
        {PIN_IN_ATX_PWR, 0U},
        {PIN_IN_ATX_PWR_OK, 0U},
        {PIN_IN_ASLEEP_3P3, 1U},
        {PIN_IN_PWR_SW, 0U},
        {PIN_IN_RCW_SEL_SW, 0U},
        {PIN_IN_RESET_REQ_IGNORE, 0U},
        {PIN_IN_RESET_REQ_B33, 1U}
};

TestGpio_t gpioOutputs[6] = {
        {PIN_OUT_PS_ON, 0U},
        {PIN_OUT_PORESET, 0U},
        {PIN_OUT_CPUVDD_EN, 0U},
        {PIN_OUT_LED_PLATFORM_FAIL, 0U},
        {PIN_OUT_LED_GREEN_DRV, 0U},
        {PIN_OUT_RCW_SRC_SEL, 0U}
};

void Sysctl_Setup(){
    printf("Sysctl setup\n");
}

void Sysctl_Teardown(){
    printf("Sysctl teardown\n");
}



static void simulate(void){
    for (int i=0;i<6;i++){

        // simulate ATX on,
        if (PIN_OUT_PS_ON == gpioOutputs[i].pin && gpioOutputs[i].value == 1){
            gpioInputs[4].value = 1; // atx power ok
            gpioInputs[1].value = 1; // 3v3 pgood
            gpioInputs[2].value = 1; // 5v0 pgood

        }

        // simualte vdd en
        if (PIN_OUT_CPUVDD_EN == gpioOutputs[i].pin && gpioOutputs[i].value == 1){
            gpioInputs[0].value = 1; // vdd pgood
            gpioInputs[3].value = 0; // ddr pgood
        }
    }
}



uint8_t TestTwiReinitCallback(void){
    printf("Test twi reinit called\n");
    return 0;
}
