
#include "CppUTest/TestHarness.h"

extern "C" {
#include "sysctl.h"
#include "flash.h"
#include "fakes/fake_flash.h"
//#include "fakes/fake_sysctl.h"
}

TEST_GROUP(SysctlTests){
    void setup() {
        Flash_Setup();
    }

    void teardown() {
        Flash_Teardown();
    }
};

TEST(SysctlTests, testStateMachine) {
    CHECK_EQUAL(0,0);
}

TEST(SysctlTests, testResetEnable) {

    ApiGetSystemState_t sysState;
    // This should initialise the resetEnable to rstEnable
    SysCtl_Init(NULL);

    SysCtl_ApiGetSystemState(NULL, (uint8_t *)&sysState);

    CHECK_EQUAL(sysState.resetEnabled, 1);
}
