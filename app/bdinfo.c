#include "bdinfo.h"
#include <string.h>
#include "printout.h"
#include "fwup.h"

uint8_t BdInfo_ApiSetMacAddr(const uint8_t * recv, uint8_t * toSend)
{
	const BdInfoMac_t * macAddr = (BdInfoMac_t *)recv;

	// validate
	if (macAddr->mac[0] != 0x00 || macAddr->mac[1] != 0x0A || macAddr->mac[2] != 0x0FA || macAddr->mac[3] != 0x24)
	{
		BDINFO_PRINT("Api set mac address, mac addr invalid\n");
	}

	// save to flash
	else if (feSuccess != Flash_StoreData(BDINFO_MAC_LOC_IN_FLASH, &macAddr->mac[0], sizeof(BdInfoMac_t)))
	{
		BDINFO_PRINT("Api set mac address, failed to save to flash\n");
	}

	else
	{

		API_PRINT("Api set mac address, mac = %02X:%02X:%02X:%02X:%02X:%02X\n",
				macAddr->mac[0],
				macAddr->mac[1],
				macAddr->mac[2],
				macAddr->mac[3],
				macAddr->mac[4],
				macAddr->mac[5]);
	}

	return 0;
}

uint8_t BdInfo_ApiGetInfo(const uint8_t * recv, uint8_t * toSend)
{

    FwupBootImageData_t imgData;
    FwupImageInfo_t imgInfo;
	BdInfoInfo_t * info = (BdInfoInfo_t *)toSend;
	memset((uint8_t *)info,0,sizeof(BdInfoInfo_t));

	Flash_ReadData(BDINFO_MAC_LOC_IN_FLASH, info->mac, sizeof(BdInfoMac_t));

	Flash_GetUid(info->cpuId);

    Fwup_ApiGetInfo(NULL, (uint8_t *)&imgInfo);

    // check which slot we are booting from
    Flash_ReadData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    if (imgData.bankId == bankId1 && imgInfo.bankAImg.imageType == imgTypeStable)
    {
        info->fwVersion.maj = imgInfo.bankAImg.versionMajor;
        info->fwVersion.min = imgInfo.bankAImg.versionMinor;
        info->fwVersion.patch = imgInfo.bankAImg.versionPatch;
    }
    else if (imgData.bankId == bankId2 && imgInfo.bankBImg.imageType == imgTypeStable)
    {
        info->fwVersion.maj = imgInfo.bankBImg.versionMajor;
        info->fwVersion.min = imgInfo.bankBImg.versionMinor;
        info->fwVersion.patch = imgInfo.bankBImg.versionPatch;
    }
    else
    {
        info->fwVersion.maj = 0;
        info->fwVersion.min = 0;
        info->fwVersion.patch = 0;
    }

	return sizeof(BdInfoInfo_t);
}

