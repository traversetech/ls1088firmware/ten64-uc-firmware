#ifndef FAKE_SYSCTL_H
#define FAKE_SYSCTL_H

#include <stdio.h>
#include <stdint.h>
#include <assert.h>



void SYSCON_AttachSignal(SYSCON_Type *base, uint32_t index, syscon_connection_t connection);
void PINT_Init(PINT_Type *base);
void PINT_PinInterruptConfig(PINT_Type *base, pint_pin_int_t intr, pint_pin_enable_t enable, pint_cb_t callback);
void PINT_EnableCallbackByIndex(PINT_Type *base, pint_pin_int_t pintIdx);

void Sysctl_Setup();
void Sysctl_Teardown();

#endif
