
#include "CppUTest/TestHarness.h"

extern "C" {
#include "api.h"
#include "bdinfo.h"
}

TEST_GROUP(ApiTests)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

TEST(ApiTests, testSetMacAddrInvalid) {

    // build a fake message
    uint8_t recv[7] = {apiBdInfoSetMac,0,0,0,0,0,0};
    uint8_t send[255];
    uint8_t txLength = 0;
    API_ProcessData(recv,send,&txLength);
    
    CHECK_EQUAL(txLength,0);
}

TEST(ApiTests, testSetMacAddrValid) {

    // build a fake message
    uint8_t recv[7] = {apiBdInfoSetMac,0x00,0x0A,0xFA,0x24,0x25,0x92};
    uint8_t send[255];
    uint8_t txLength = 0;
    API_ProcessData(recv,send,&txLength);
    CHECK_EQUAL(txLength,0);
    
    recv[0] = apiBdInfoGetInfo;
    API_ProcessData(recv,send,&txLength);
    printf("\nRaw data: ");
    for (int i=0;i<txLength;i++)
        printf("[%02X]",send[i]);
    printf("\n");
    
    CHECK_EQUAL(txLength, sizeof(BdInfoInfo_t) + 1);
    CHECK_EQUAL(send[0],recv[0]);
    MEMCMP_EQUAL(&send[1], &recv[1], 6);
}
