#ifndef FAKE_DRIVERS_H
#define FAKE_DRIVERS_H

#include <stdint.h>

#define SYSCON			(void *)0
#define PINT			(void *)0
#define kPINT_PinInt1   0
#define GPIO 			(void *)0

#define	false	0
#define	true	1

#define kSYSCON_GpioPort0Pin20ToPintsel 0U



typedef enum _gpio_pin_direction {
    kGPIO_DigitalInput = 0U,
    kGPIO_DigitalOutput = 1U,
} gpio_pin_direction_t;

typedef struct _gpio_pin_config {
    gpio_pin_direction_t pinDirection;
    uint8_t outputLogic;
} gpio_pin_config_t;

typedef struct {
    uint8_t dummy;
} GPIO_Type;


typedef struct {
    uint8_t dummy;
} SYSCON_Type;

typedef enum {
    sysconDummy
} syscon_connection_t;

typedef struct {
    uint8_t dummy;
} PINT_Type;

typedef enum {
    pinIntDummy
} pint_pin_int_t;

typedef enum {
    kPINT_PinIntEnableBothEdges = 0U
} pint_pin_enable_t;

typedef void (*pint_cb_t)(pint_pin_int_t pintr, uint32_t pmatch_status);


void SYSCON_AttachSignal(SYSCON_Type *base, uint32_t index, syscon_connection_t connection);
void PINT_Init(PINT_Type *base);
void PINT_PinInterruptConfig(PINT_Type *base, pint_pin_int_t intr, pint_pin_enable_t enable, pint_cb_t callback);
void PINT_EnableCallbackByIndex(PINT_Type *base, pint_pin_int_t pintIdx);
void GPIO_PortInit(GPIO_Type *base, uint32_t port, uint32_t pin, const gpio_pin_config_t *config);
uint32_t GPIO_PinRead(GPIO_Type *base, uint32_t port, uint32_t pin);
void GPIO_PinWrite(GPIO_Type *base, uint32_t port, uint32_t pin, uint8_t output);

#endif
