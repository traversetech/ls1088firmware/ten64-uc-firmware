#include "flash.h"
#include <stdint.h>
#include <string.h>
#include <cmsis_gcc.h>
#include "printout.h"



/* IAP Call */
typedef void (*IAP_Entry) (uint32_t *cmd, uint32_t *stat);
#define IAP_Call ((IAP_Entry) IAP_LOCATION)

static inline void Flash_IapEntry(uint32_t *cmd_param, uint32_t *result)
{
    __disable_irq();
    IAP_Call(cmd_param, result);
    __enable_irq();
}

static uint16_t Flash_GetPageNumber(uint16_t addr){
	return (uint16_t)(addr/FLASH_PAGE_SIZE);
}

static uint16_t Flash_GetSectorNumber(uint16_t addr) {
    return (uint16_t)(addr/FLASH_SECTOR_SIZE);
}

static uint8_t Flash_ErasePage(uint16_t addr) {
	uint32_t command[5];
	uint32_t result[5];
	uint32_t sectorNum;
	uint32_t pageNum;

    sectorNum = (uint32_t)Flash_GetSectorNumber(addr);

    command[0] = IAP_COMMAND_PREPARE_SECTOR;
    command[1] = sectorNum;
    command[2] = sectorNum;

    Flash_IapEntry(command, result);
    if (result[0] != 0) {
    	FLASH_PRINT("FEP_p: e=%d\n", result[0]);
		return (uint8_t)result[0];
	}

    pageNum = (uint32_t)Flash_GetPageNumber(addr);

    command[0] = IAP_COMMAND_ERASE_PAGE;
    command[1] = pageNum;
    command[2] = pageNum;
    command[3] = FLASH_CCLK;

    Flash_IapEntry(command, result);
	if (result[0] != 0) {
		FLASH_PRINT("FEP_e: e=%d\n", result[0]);
		return (uint8_t)result[0];
	}

    return feSuccess;
}

uint8_t Flash_ReadSignature(uint32_t startPage, uint32_t endPage, uint32_t numOfState, uint32_t *signature)
{
    uint32_t command[5], result[5];

    command[0] = (uint32_t)IAP_COMMAND_EXTENDED_READ_SIGN;
    command[1] = startPage;
    command[2] = endPage;
    command[3] = numOfState;
    command[4] = 0;
    Flash_IapEntry(command, result);
    signature[0] = result[4];
    signature[1] = result[3];
    signature[2] = result[2];
    signature[3] = result[1];

    return (uint8_t)result[0];
}

uint8_t Flash_EraseSector(uint32_t addr) {
	uint32_t command[5];
	uint32_t result[5];
	uint32_t sectorNum;

	sectorNum = (uint32_t)Flash_GetSectorNumber(addr);
	command[0] = IAP_COMMAND_PREPARE_SECTOR;
	command[1] = sectorNum;
	command[2] = sectorNum;

	Flash_IapEntry(command, result);
	if (result[0] != 0) {
		FLASH_PRINT("FES_p: e=%d\n", result[0]);
		return (uint8_t)result[0];
	}

	command[0] = IAP_COMMAND_ERASE_SECTOR;
	command[1] = sectorNum;
	command[2] = sectorNum;
	command[3] = FLASH_CCLK;

	Flash_IapEntry(command, result);
	if (result[0] != 0) {
		FLASH_PRINT("FES_e: e=%d\n", result[0]);
		return (uint8_t)result[0];
	}

	return feSuccess;
}

/**
 * NOTE: Length should be at least a page i.e. 64B
 */
uint8_t Flash_Write(uint32_t addr, const uint8_t *data, const uint16_t length) {
  	uint32_t command[5];
	uint32_t result[5];
	uint32_t sectorNum;

	sectorNum = Flash_GetSectorNumber(addr);

	command[0] = IAP_COMMAND_PREPARE_SECTOR;
	command[1] = sectorNum;
	command[2] = sectorNum;

	Flash_IapEntry(command, result);
	if (result[0] != 0) {
		FLASH_PRINT("FW_p: e=%d\n", result[0]);
		return (uint8_t)result[0];
	}

	command[0] = IAP_COMMAND_WRITE;
	command[1] = addr;
	command[2] = (uint32_t)data;
	command[3] = (uint32_t)length;
	command[4] = FLASH_CCLK;

	Flash_IapEntry(command, result);
	if (result[0] != 0) {
		FLASH_PRINT("FW_w: e=%d\n", result[0]);
		return (uint8_t)result[0];
	}

	return feSuccess;
}

uint8_t Flash_Compare(uint32_t dstAddr, uint32_t * srcAddr, uint16_t numOfBytes) {
    uint32_t command[5];
    uint32_t result[5];

    command[0] = (uint32_t)IAP_COMMAND_COMPARE;
    command[1] = dstAddr;
    command[2] = (uint32_t)srcAddr;
    command[3] = (uint32_t)numOfBytes;

    Flash_IapEntry(command, result);

    if (result[0] != 0) {
    	FLASH_PRINT("FC: e=%d\n", result[0]);
		return (uint8_t)result[0];
    }

    return feSuccess;
}

uint8_t Flash_GetUid(uint32_t * uid){
	uint32_t command[5];
	uint32_t result[5];

	command[0] = IAP_COMMAND_READ_UID;
	Flash_IapEntry(command, result);

	memcpy(uid, &result[1], (sizeof(uint32_t)*4));
	return feSuccess;
}

uint8_t Flash_BlankCheckSector(uint32_t startSector, uint32_t endSector)
{
    uint32_t command[5];
    uint32_t result[5];

    command[0] = (uint32_t)IAP_COMMAND_BLANK_CHECK_SECTOR;
    command[1] = startSector;
    command[2] = endSector;

    Flash_IapEntry(command, result);

    if (result[0] != 0) {
		FLASH_PRINT("FBSC: e=%d\n", result[0]);
		return (uint8_t)result[0];
	}

	return feSuccess;
}

uint8_t Flash_ReadData(uint32_t addr, uint8_t * data, uint16_t length) {
	memcpy(data,(uint8_t *)addr,length);
	return feSuccess;
}

uint8_t Flash_StoreData(const uint32_t addr, const uint8_t * data, const uint16_t length){

	// 0 or greater than a sector
	if (length == 0 || length > FLASH_SECTOR_SIZE){
		FLASH_PRINT("Flash_StoreData, invalid length %ld\n",length);
		return feStoreLengthInvalid;
	}

	// less than a page
	if (length % FLASH_PAGE_SIZE){
		uint8_t page[FLASH_PAGE_SIZE];
		memset(page,0xff,FLASH_PAGE_SIZE);
		for (int i=0;i<length;i++)
			page[i]=data[i];

		if (feSuccess != Flash_ErasePage(addr)){
			FLASH_PRINT("Flash_StoreData, failed to erase page addr = %04X\n", addr);
			return feErasePageFailed;
		}

		if (feSuccess != Flash_Write(addr, page, (uint32_t)FLASH_PAGE_SIZE)){
			FLASH_PRINT("Flash_StoreData, failed to write data at addr = %04X\n", addr);
			return feWritePageFailed;
		}
	} else {

		uint32_t pages = length/FLASH_PAGE_SIZE;
		for (uint32_t i=0; i<pages; i++){
			uint32_t pageAddr = addr + FLASH_PAGE_SIZE*i;
			if (feSuccess != Flash_ErasePage(pageAddr)){
				FLASH_PRINT("Flash_StoreData, failed to erase page addr = %04X\n", pageAddr);
				return feErasePageFailed;
			}
		}

		if (feSuccess != Flash_Write(addr, data, (uint32_t)length)){
			FLASH_PRINT("Flash_StoreData, failed to write data at addr = %04X\n", addr);
			return feWritePageFailed;
		}
	}

	FLASH_PRINT("Flash_StoreData, store data of length %ld at addr = %04X\n",length,addr);

	return feSuccess;
}

uint32_t *Flash_PointData(uint32_t addr){
    return (uint32_t *)addr;
}
