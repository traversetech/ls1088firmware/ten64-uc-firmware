#include "flash.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

static uint8_t fakeFlashMemory[0x8000];

void Flash_Setup(){
    printf("Fake flash setup\n");
    memset(fakeFlashMemory,0,0x8000);
}

void Flash_Teardown(){
    printf("Teardown fake flash\n");
}

uint8_t Flash_EraseSector(uint32_t addr){
    memset(fakeFlashMemory+addr,0xFF,1024);
    printf("Erase sector %08X\n", addr);
    /*for (int i=0;i<1024;i++){
        printf("[%02X]",*(fakeFlashMemory+addr));
    }
    printf("\n");*/
    return feSuccess;
}

uint8_t Flash_Write(uint32_t addr, const uint8_t * data, const uint16_t length){
    printf("Write data to addr=%08X\n",addr);
    uint8_t * dst = (uint8_t *)(fakeFlashMemory + addr);
    memcpy(dst,data,length);
    return feSuccess;
}

uint8_t Flash_BlankCheckSector(uint32_t startSector, uint32_t endSector){
    uint8_t blank[1024];
    memset(blank,0xFF,1024);


    uint8_t * src = (uint8_t *)(fakeFlashMemory + startSector*1024);

    if (memcmp(src,blank,1024) == 0)
        return feSuccess;
    else
        return feBlankCheckFailed;
}

uint8_t Flash_Compare(uint32_t dstAddr, uint32_t *srcAddr, uint16_t numOfBytes){
    return feSuccess;
}

uint8_t Flash_GetUid(uint32_t * uid){
    return feSuccess;
}

uint8_t Flash_StoreData(uint32_t addr, const uint8_t * data, const uint16_t length){
    //uint32_t * addrLocation = fakeFlashMemory;
    //addrLocation += addr;
    //printf("Fake flash mem store data @ %p\n",addrLocation);
    memcpy(fakeFlashMemory+addr,data,length);
    return feSuccess;
}

uint8_t Flash_ReadData(uint32_t addr, uint8_t * data, uint16_t length){
   // uint32_t * addrLocation = fakeFlashMemory;
   // addrLocation += addr;
   // printf("Fake flash mem read data @ %p\n",addrLocation);
    memcpy(data,fakeFlashMemory+addr,length);
    return feSuccess;
}

uint32_t * Flash_PointData(uint32_t addr){
    return (uint32_t *)(fakeFlashMemory + addr);
}
