/*
 * fwup.c
 *
 *  Created on: 13 Feb 2021
 *      Author: vaughn
 */


#include <string.h>
#include "fwup.h"
#include "printout.h"
#include "flash.h"
#include "timer.h"
#include "image.h"
#include "reset.h"

FwupPrivate_t fwupPriv;

uint8_t Fwup_ApiGetInfo(const uint8_t * recv, uint8_t * toSend){

    FwupImageInfo_t * info = (FwupImageInfo_t *)toSend;

    Flash_ReadData(FWUP_APP1_HDR_LOCATION_IN_FLASH, (uint8_t *)&info->bankAImg, sizeof(FwupImageHeader_t));
    Flash_ReadData(FWUP_APP2_HDR_LOCATION_IN_FLASH, (uint8_t *)&info->bankBImg, sizeof(FwupImageHeader_t));

    return sizeof(FwupImageInfo_t);
}

uint8_t Fwup_ApiInit(const uint8_t * recv, uint8_t * toSend){

    uint32_t imgAddr;
    fwupPriv.totalBlocksReceived = 0;
    fwupPriv.totalBlocksFlashed = 0;
    FwupImageInfo_t imgInfo;

    FwupRunState_t * fwState = (FwupRunState_t *)toSend;
    fwState->error = fwupSuccess;
    fwState->state = stateInit;

    if (recv[0] == imgInBankA){
        fwupPriv.location = imgInBankA;
        imgAddr = FWUP_APP1_LOCATION_IN_FLASH;

        // don't allow erase of stable firmware
        Flash_ReadData(FWUP_APP1_HDR_LOCATION_IN_FLASH, (uint8_t *)&imgInfo.bankAImg, sizeof(FwupImageHeader_t));
        if (imgInfo.bankAImg.imageType == imgTypeStable){
            fwState->error = fwupErrEraseStable;
            goto DONE;
        }

    } else if (recv[0] == imgInBankB) {
        fwupPriv.location = imgInBankB;
        imgAddr = FWUP_APP2_LOCATION_IN_FLASH;

        // don't allow erase of stable firmware
        Flash_ReadData(FWUP_APP2_HDR_LOCATION_IN_FLASH, (uint8_t *)&imgInfo.bankBImg, sizeof(FwupImageHeader_t));
        if (imgInfo.bankBImg.imageType == imgTypeStable){
            fwState->error = fwupErrEraseStable;
            goto DONE;
        }
    } else {
        fwState->error = fwupErrBankIdInvalid;
        goto DONE;
    }

    // erase the flash bank
    for (int i=0; i<FWUP_APP_BANK_SIZE_SECTORS; i++){
        uint32_t addr = imgAddr + (uint32_t)(FLASH_SECTOR_SIZE*i);

        if (Flash_EraseSector(addr) != feSuccess){
            fwState->error = fwupErrEraseSector;
            FWUP_PRINT("Fwup erase sector error, sector %d\n",i);
            goto DONE;
        }

        uint32_t sector_number = addr/FLASH_SECTOR_SIZE;
        if (Flash_BlankCheckSector(sector_number,sector_number) != feSuccess){
            fwState->error = fwupErrBlankCheckSector;
            FWUP_PRINT("Fwup blank check sector error, sector %d\n",i);
            goto DONE;
        }
    }
    FWUP_PRINT("Erase app flash bank done\n");

DONE:
    return sizeof(FwupRunState_t);
}

uint8_t Fwup_ApiTransfer(const uint8_t * recv, uint8_t * toSend){

    uint32_t addr;
    uint8_t rc;
    uint8_t dataBlock[FWUP_XFER_BLOCK_SIZE];
    FwupTransferResult_t * result = (FwupTransferResult_t *)toSend;

    if (fwupPriv.location == imgInBankA){
        addr = FWUP_APP1_LOCATION_IN_FLASH + (uint32_t)(fwupPriv.totalBlocksReceived * FWUP_XFER_BLOCK_SIZE);
    } else if (fwupPriv.location == imgInBankB) {
        addr = FWUP_APP2_LOCATION_IN_FLASH + (uint32_t)(fwupPriv.totalBlocksReceived * FWUP_XFER_BLOCK_SIZE);
    } else {
        result->error = fwupErrBankIdInvalid;
        goto DONE;
    }

    result->addr = addr;
    result->currentBlock = fwupPriv.totalBlocksReceived;

    fwupPriv.totalBlocksReceived++;

    FWUP_PRINT("FlashWrite: addr = %08X\n", addr);

    // write to flash
    memcpy(dataBlock, recv, FWUP_XFER_BLOCK_SIZE);
    rc = Flash_Write(addr, dataBlock, FWUP_XFER_BLOCK_SIZE);
    if ( rc != feSuccess){
        result->error = rc;
        FWUP_PRINT("Fwup write error, block %d\n",fwupPriv.totalBlocksReceived);
        goto DONE;
    }

    // compare
    rc = Flash_Compare(addr, (uint32_t *)dataBlock, FWUP_XFER_BLOCK_SIZE);
    if (rc != feSuccess){
        result->error = 10 + rc;
        FWUP_PRINT("Fwup compare error, block %d\n",fwupPriv.totalBlocksReceived);
        goto DONE;
    }

    fwupPriv.totalBlocksFlashed++;

    FWUP_PRINT("Blocks recv %d, blocks flashed %d\n",fwupPriv.totalBlocksReceived, fwupPriv.totalBlocksFlashed);

    result->error = fwupSuccess;

DONE:
    return sizeof(FwupTransferResult_t);
}

/**
 * @brief Fwup_ApiCheck
 * @param recv
 * @param toSend
 * @return Size of transmit bytes to send (0 for none)
 * @details The header is in recv. Write it to flash and verify the image.
 *          If the verifification fails, invalidate the header and set the response in state.
 */
uint8_t Fwup_ApiCheck(const uint8_t *recv, uint8_t *toSend) {

    uint8_t dataBlock[FWUP_XFER_BLOCK_SIZE];
    uint32_t * imgAddr;
    uint32_t imgHeaderAddr;
    FwupImageHeader_t * imgHdrPtr;
    uint8_t rc;

    FwupRunState_t * fwState = (FwupRunState_t *)toSend;
    fwState->state = stateCheck;

    FWUP_PRINT("Recv header:\n");
    for (int i=0;i<64;i++)
        FWUP_PRINT("[%02X]",recv[i]);
    FWUP_PRINT("\n");


    if (fwupPriv.location == imgInBankA){
        imgAddr = Flash_PointData(FWUP_APP1_LOCATION_IN_FLASH);
        imgHeaderAddr = (uint32_t)(FWUP_APP1_HDR_LOCATION_IN_FLASH);
    } else if (fwupPriv.location == imgInBankB) {
        imgAddr = Flash_PointData(FWUP_APP2_LOCATION_IN_FLASH);
        imgHeaderAddr = (uint32_t)(FWUP_APP2_HDR_LOCATION_IN_FLASH);
    } else {
        fwState->error = fwupErrBankIdInvalid;
        goto DONE;
    }

    FWUP_PRINT("ImgHeaderaddr = %08X\n",imgHeaderAddr);
    // verify image

    rc = Image_Verify(imgAddr, (FwupImageHeader_t *)recv);

    if (rc == imgVerifyErrMagic) {
        fwState->error = fwupErrCheckNoMagic;
        FWUP_PRINT("Fwup check no header magic\n");
        goto DONE;
    } else if (rc == imgVerifyErrCrc) {
        fwState->error = fwupErrCheckCrcFailed;
        FWUP_PRINT("Fwup check crc failed\n");
        goto DONE;
    }

    // write to flash i.e. commit the header and validate the image
    memcpy(dataBlock, recv, FWUP_XFER_BLOCK_SIZE);

    if (Flash_Write(imgHeaderAddr, dataBlock, FWUP_XFER_BLOCK_SIZE) != feSuccess){
        fwState->error = fwupErrWrite;
        FWUP_PRINT("Fwup write header error\n");
        goto DONE;
    }

    // compare
    if (Flash_Compare(imgHeaderAddr, (uint32_t *)dataBlock, FWUP_XFER_BLOCK_SIZE) != feSuccess){
        fwState->error = fwupErrCompare;
        FWUP_PRINT("Fwup compare header error\n");
        goto DONE;
    }

    imgHdrPtr = Flash_PointData(imgHeaderAddr);
    FWUP_PRINT("Fwup check success\n");
    if (fwupPriv.location == imgInBankA)
        FWUP_PRINT("Firmware in bank A\n");
    else
        FWUP_PRINT("Firmware in bank B\n");
    FWUP_PRINT("    Version: %d.%d.%d\n",imgHdrPtr->versionMajor,imgHdrPtr->versionMinor,imgHdrPtr->versionPatch);
    FWUP_PRINT("    Git sha: %s\n",imgHdrPtr->gitShortSHA);
    FWUP_PRINT("    Size: %dB\n",imgHdrPtr->imageLength);

    fwState->error = fwupSuccess;

DONE:
    return sizeof(FwupRunState_t);
}

uint8_t Fwup_ApiBoot(const uint8_t *recv, uint8_t *toSend) {
    FwupBootImageData_t imgData;
    imgData.preamb = FWUP_IMAGE_MAGIC;
    imgData.bootCount = 0;
    Flash_StoreData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    // force reset
    Lpc_Reset();

    return 0;
}


