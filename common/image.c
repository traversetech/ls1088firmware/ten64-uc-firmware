/*
 * image.c
 *
 *  Created on: 16 Feb 2021
 *      Author: vaughn
 */

#include <string.h>
#include "flash.h"
#include "crc32.h"
#include "printout.h"
#include "image.h"
#include "timer.h"



/**
 * Loads the stack pointer address from the vector table and
 * jumps to the reset handler
 */
#ifndef UTEST
__attribute__((naked))
static void Image_Boot(uint32_t pc, uint32_t sp)  {

    __asm("           \n\
          msr msp, r1 /* load r1 into MSP */\n\
          bx r0       /* branch to the address at r0 */\n\
    ");
}
#else
static void Image_Boot(uint32_t pc, uint32_t sp)  {
    IMAGE_PRINT("Running application now\n\n\n\n\n");
}
#endif


/**
 * @brief Image_Verify
 * @param imgHeader
 * @note imgHeader points to memory address of base application firmware
 * @return
 */
uint8_t Image_Verify(const uint32_t * imgStartAddr, const FwupImageHeader_t * imgHeader){

	uint32_t crc;

    if (imgHeader->magic != FWUP_IMAGE_MAGIC){
    	IMAGE_PRINT("Image_Verify: No magic\n");
    	return imgVerifyErrMagic;
    }

    // move to end of header and calculate crc
    crc = crc32((void *)(imgStartAddr), (uint32_t)imgHeader->imageLength);

    if (imgHeader->crc != crc){
        IMAGE_PRINT("Image_Verify: Crc mismatch, calc %08X, expect %08X\n",crc,imgHeader->crc);
		return imgVerifyErrCrc;
    }

    IMAGE_PRINT("Image_Verify: Success\n");
	return imgVerifySuccess;
}

void Image_SetBootCount(const uint8_t bootCount){
	FwupBootImageData_t imageData;
	imageData.bootCount = bootCount;
	imageData.preamb = FWUP_IMAGE_MAGIC;
    IMAGE_PRINT("Image_SetBootCount: Set boot count to %d\n",imageData.bootCount);
	Flash_StoreData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imageData, sizeof(FwupBootImageData_t));
}

void Image_UpdateHeader(const FwupBankId_t bankId, const FwupImageType_t type){

    FwupImageHeader_t imgHeader;
    uint32_t addr;

    if (bankId == bankId1)
        addr = FWUP_APP1_HDR_LOCATION_IN_FLASH;
    else if (bankId == bankId2)
        addr = FWUP_APP2_HDR_LOCATION_IN_FLASH;
    else {
        IMAGE_PRINT("Update image header: Bank id not valid\n");
        return;
    }

    Flash_ReadData(addr, (uint8_t *)&imgHeader, sizeof(FwupImageHeader_t));

    if (imgHeader.magic != FWUP_IMAGE_MAGIC){
        IMAGE_PRINT("Update image header: No image magic\n");
        return;
    }

    imgHeader.imageType = type;

    Flash_StoreData(addr, (uint8_t *)&imgHeader, sizeof(FwupImageHeader_t));
}

void Image_SetAsStable(void){

    // check which slot we are running from
    FwupImageHeader_t * imgHeader;
    FwupBootImageData_t imgData;
    FwupImageInfo_t imgInfo;

    Flash_ReadData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    if (imgData.bankId == imgInBankA){
        imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP1_HDR_LOCATION_IN_FLASH);
    } else {
        imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP2_HDR_LOCATION_IN_FLASH);
    }

    //IMAGE_PRINT("App stability: bank=%d, imgtype=%d, version=v%d.%d.%d\n", imgData.bankId, imgHeader->imageType,
    //            imgHeader->versionMajor, imgHeader->versionMinor, imgHeader->versionPatch);

    // already stable
    if (imgHeader->imageType == imgTypeStable || imgHeader->imageType == imgTypeForceOldStable){
        IMAGE_PRINT("Image set as stable: App is already stable\n");

    // if candidate, set to stable
    } else if (imgData.bankId == imgInBankA && imgHeader->imageType == imgTypeCandidate){
        IMAGE_PRINT("Image set as stable: Set candidate app in bank A as stable\n");
        Image_UpdateHeader(imgInBankA, imgTypeStable);

        // read header in b and set it to old stable if it was previously stable
        FwupImageHeader_t * oldStableHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP2_HDR_LOCATION_IN_FLASH);

        if (oldStableHeader->imageType == imgTypeStable){
            IMAGE_PRINT("Image set as stable: Previous stable in bank B, version=v%d.%d.%d. Now set it as old stable\n",
                        oldStableHeader->versionMajor, oldStableHeader->versionMinor, oldStableHeader->versionPatch);

            Image_UpdateHeader(imgInBankB, imgTypeOldStable);
        }

    } else if (imgData.bankId == imgInBankB && imgHeader->imageType == imgTypeCandidate){
        Image_UpdateHeader(imgInBankB, imgTypeStable);
        IMAGE_PRINT("Image set as stable: Set candidate app in bank B as stable\n");

        // read header in a and set it to old stable if it was previously stable
        FwupImageHeader_t * oldStableHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP1_HDR_LOCATION_IN_FLASH);

        if (oldStableHeader->imageType == imgTypeStable){
            IMAGE_PRINT("Image set as stable: Previous stable in bank A, version=v%d.%d.%d. Now set it as old stable\n",
                        oldStableHeader->versionMajor, oldStableHeader->versionMinor, oldStableHeader->versionPatch);
            Image_UpdateHeader(imgInBankA, imgTypeOldStable);
        }
    }


    // update boot count
    imgData.bootCount = 0;
    Flash_StoreData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    Fwup_ApiGetInfo(NULL, (uint8_t *)&imgInfo);
    IMAGE_PRINT("Bank A:\n");
    IMAGE_PRINT("   Type = %d\n", imgInfo.bankAImg.imageType);
    IMAGE_PRINT("   Crc = %08X\n", imgInfo.bankAImg.crc);
    IMAGE_PRINT("   Length = %d\n", imgInfo.bankAImg.imageLength);
    IMAGE_PRINT("   Sha = %s\n", imgInfo.bankAImg.gitShortSHA);
    IMAGE_PRINT("   Version = v%d.%d.%d\n", imgInfo.bankAImg.versionMajor, imgInfo.bankAImg.versionMinor, imgInfo.bankAImg.versionPatch);

    IMAGE_PRINT("Bank B:\n");
    IMAGE_PRINT("   Type = %d\n", imgInfo.bankBImg.imageType);
    IMAGE_PRINT("   Crc = %08X\n", imgInfo.bankBImg.crc);
    IMAGE_PRINT("   Length = %d\n", imgInfo.bankBImg.imageLength);
    IMAGE_PRINT("   Sha = %s\n", imgInfo.bankBImg.gitShortSHA);
    IMAGE_PRINT("   Version = v%d.%d.%d\n", imgInfo.bankBImg.versionMajor, imgInfo.bankBImg.versionMinor, imgInfo.bankBImg.versionPatch);
}

void Image_LoadVerifyRun(void) {
	FwupBootImageData_t imgData;
	FwupImageHeader_t * imgHeader;
    uint32_t * imgStartAddr;

	Flash_ReadData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    if (imgData.preamb != FWUP_IMAGE_MAGIC){
        IMAGE_PRINT("No preamb found\n");
        imgData.bootCount = 0;
        imgData.preamb = FWUP_IMAGE_MAGIC;
    }

    IMAGE_PRINT("Image_LoadVerifyRun: Current boot count %d\n",imgData.bootCount);

    // first look for candidate to boot
	for (uint8_t imgSlot=imgInBankA; imgSlot<=imgInBankB; imgSlot++){

        if (imgSlot == imgInBankA){
            imgStartAddr = Flash_PointData(FWUP_APP1_LOCATION_IN_FLASH);
            imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP1_HDR_LOCATION_IN_FLASH);
        } else {
            imgStartAddr = Flash_PointData(FWUP_APP2_LOCATION_IN_FLASH);
            imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP2_HDR_LOCATION_IN_FLASH);
        }

        // verify first
        if (Image_Verify(imgStartAddr, imgHeader) != imgVerifySuccess)
            continue;

        // boot candidate
        if (imgHeader->imageType == imgTypeCandidate && imgData.bootCount < 3){

            // increment boot count
            ++imgData.bootCount;
            imgData.bankId = imgSlot;
            Flash_StoreData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

            // boot this image
            IMAGE_PRINT("Image_LoadVerifyRun: Booting candidate\n");
            IMAGE_PRINT("   Version: %d.%d.%d\n",imgHeader->versionMajor, imgHeader->versionMinor, imgHeader->versionPatch);
            IMAGE_PRINT("   Sha: %s\n",imgHeader->gitShortSHA);

            Image_Boot(imgStartAddr[1], imgStartAddr[0]);

        // set candidate as unstable as boot count reached 3
        } else if (imgHeader->imageType == imgTypeCandidate) {

            IMAGE_PRINT("Image_LoadVerifyRun: Setting candidate (v%d.%d.%d [%s]) as unstable after 3 failed boot attempts\n",
                        imgHeader->versionMajor,
                        imgHeader->versionMinor,
                        imgHeader->versionPatch,
                        imgHeader->gitShortSHA);
            Image_UpdateHeader(imgSlot, imgTypeUnstable);


        }
    }

    // no candidate, now look for stable to boot
    for (uint8_t imgSlot=imgInBankA; imgSlot<=imgInBankB; imgSlot++){

        if (imgSlot == imgInBankA){
            imgStartAddr = Flash_PointData(FWUP_APP1_LOCATION_IN_FLASH);
            imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP1_HDR_LOCATION_IN_FLASH);
        } else {
            imgStartAddr = Flash_PointData(FWUP_APP2_LOCATION_IN_FLASH);
            imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP2_HDR_LOCATION_IN_FLASH);
        }

        // verify first
        if (Image_Verify(imgStartAddr, imgHeader) != imgVerifySuccess)
            continue;

        if (imgHeader->imageType == imgTypeStable) {

            IMAGE_PRINT("Image_LoadVerifyRun: Booting stable\n");
            IMAGE_PRINT("   Version: %d.%d.%d\n",imgHeader->versionMajor, imgHeader->versionMinor, imgHeader->versionPatch);
            IMAGE_PRINT("   Sha: %s\n",imgHeader->gitShortSHA);

            imgData.bankId = imgSlot;
            imgData.bootCount = 0;
            Flash_StoreData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

            Image_Boot(imgStartAddr[1], imgStartAddr[0]);

        }

	}

    // no candidata and no stable, now look for old stable to boot
    for (uint8_t imgSlot=imgInBankA; imgSlot<=imgInBankB; imgSlot++){

        if (imgSlot == imgInBankA){
            imgStartAddr = Flash_PointData(FWUP_APP1_LOCATION_IN_FLASH);
            imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP1_HDR_LOCATION_IN_FLASH);
        } else {
            imgStartAddr = Flash_PointData(FWUP_APP2_LOCATION_IN_FLASH);
            imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP2_HDR_LOCATION_IN_FLASH);
        }

        // verify first
        if (Image_Verify(imgStartAddr, imgHeader) != imgVerifySuccess)
            continue;

        if (imgHeader->imageType == imgTypeOldStable) {

            IMAGE_PRINT("Image_LoadVerifyRun: Booting old stable\n");
            IMAGE_PRINT("   Version: %d.%d.%d\n",imgHeader->versionMajor, imgHeader->versionMinor, imgHeader->versionPatch);
            IMAGE_PRINT("   Sha: %s\n",imgHeader->gitShortSHA);

            imgData.bankId = imgSlot;
            imgData.bootCount = 0;
            Flash_StoreData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

            Image_Boot(imgStartAddr[1], imgStartAddr[0]);

        }

    }

    IMAGE_PRINT("Image_LoadVerifyRun: Shouldn't reach here\n");
}
