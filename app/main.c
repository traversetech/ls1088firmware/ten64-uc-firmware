/* Ten64 System Controller
 * Copyright 2019 Traverse Technologies
 *
 * SPDX-License-Identifier: TBA
 */

#include "api.h"
#include "bdinfo.h"
#include "sysctl.h"
#include "timer.h"
#include "twi.h"
#include <string.h>
#include "flash.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "printout.h"
#include "image.h"

static uint8_t appStable;

#if TEST_CRASH_ENABLE
#define TEST_CRASH	{int * ptr = 0; (*ptr)++;}
#else
#define TEST_CRASH
#endif


#define APP_RUNNING_FOR_1_MINUTE    60000

#define PRINT_FW(fw)				MAIN_PRINT("FW: v%d.%d.%d\n", fw.maj, fw.min, fw.patch)
#define PRINT_UID(uid)				MAIN_PRINT("UID: %08X %08X %08X %08X\n", uid[0], uid[1], uid[2], uid[3])
#define PRINT_MAC(mac) 				MAIN_PRINT("MAC address: %02X:%02X:%02X:%02X:%02X:%02X\n", \
										mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);

#ifdef NDEBUG
void HardFault_Handler(void)
{
    FwupImageHeader_t * imgHeader;
    FwupBootImageData_t imgData;
    Flash_ReadData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    if (imgData.bankId == imgInBankA){
        imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP1_HDR_LOCATION_IN_FLASH);
    } else {
        imgHeader = (FwupImageHeader_t *)Flash_PointData(FWUP_APP2_HDR_LOCATION_IN_FLASH);
    }

    if (imgHeader->imageType == imgTypeCandidate){
        Image_UpdateHeader(imgData.bankId, imgTypeUnstable);
    }

	// set vector key to 0x5FA and request system reset to AIRCR register
	SCB->AIRCR = (0x5FA<<SCB_AIRCR_VECTKEY_Pos)| SCB_AIRCR_SYSRESETREQ_Msk;
	while (1) {}
}
#endif





void main(void) {
    appStable = 0;
	FwupVersion_t fw;
	uint32_t uid[4];
	BdInfoMac_t macAddr;
    uint32_t elapsed;
    FwupBootImageData_t imgData;
    FwupImageInfo_t imgInfo;

    /* Select the main clock as source clock of I2C0. */
    CLOCK_Select(kI2C0_Clk_From_MainClk);

    /* Board pin init */
    BOARD_InitPins();
    BOARD_InitBootClocks();

    Fwup_ApiGetInfo(NULL, (uint8_t *)&imgInfo);
    Flash_ReadData(FWUP_BOOT_IMAGE_DATA_IN_FLASH, (uint8_t *)&imgData, sizeof(FwupBootImageData_t));

    if (imgData.bankId == bankId1 && imgInfo.bankAImg.imageType == imgTypeStable){
        fw.maj = imgInfo.bankAImg.versionMajor;
        fw.min = imgInfo.bankAImg.versionMinor;
        fw.patch = imgInfo.bankAImg.versionPatch;
    } else if (imgData.bankId == bankId2 && imgInfo.bankBImg.imageType == imgTypeStable){
        fw.maj = imgInfo.bankBImg.versionMajor;
        fw.min = imgInfo.bankBImg.versionMinor;
        fw.patch = imgInfo.bankBImg.versionPatch;
    } else {
        fw.maj = 0;
        fw.min = 0;
        fw.patch = 0;
    }
    PRINT_FW(fw);

    Flash_GetUid(uid);
    PRINT_UID(uid);

    Flash_ReadData(BDINFO_MAC_LOC_IN_FLASH, macAddr.mac, sizeof(BdInfoMac_t));
    PRINT_MAC(macAddr.mac);

    /* Set systick reload value to generate 1ms interrupt */
    if (SysTick_Config(SystemCoreClock / 1000)) { while(1) {}}

    // initialise twi
    Twi_Init(API_ProcessData);

    // delay 25ms
    Timer_DelayMs(25U);

    // initialise sysctl io
    SysCtl_Init(Twi_Reinit);

    // mark time so we can check stability of new firmware
    Timer_Start(tmrMain);

    while (1){
        SysCtl_Run();

        // mark candidate image as stable
        if (!appStable){
            elapsed = Timer_Elapsed(tmrMain);

            if (elapsed >= APP_RUNNING_FOR_1_MINUTE){
            	printf("Timer GetElapsed main elapsed=%d\n", elapsed);
                Image_SetAsStable();
                appStable = 1;
            }
        }
    }
}
