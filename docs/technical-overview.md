# Ten64 early boot control uC

Runs on the Nxp LPC804 Cortex M0+ with 32kB on board flash and 4kB RAM.

## Functionality

On power on, the uC incrementally and safely powers up the system before asserting the power on reset (POR) of the Ls1088 processor.

Some of the core application features include:
 
1. Boot source management by control of the reset configuration word (RCW) of the Ls1088 process.

2. External/internal reset functionality.

3. Setting the MAC address.

4. Microcontoller firmware upgraes.

## Code description

A secondary bootloader (SBL) checks, verifies and boots the main application firmware stored in flash. It marks firmware candidates as stable or unstable if the firmware fails to boot correctly and rolls back to a stable firmware.

The main application firmware manages the early boot and power up sequence of the main processor. An I2C endpoint exposes an API that can be accessed by a u-boot driver and a dedicated CLI tool for Linux.

A firmware upgrade service is also embedded into the application code to allow the microcontroller firmware to be upgraded.

## Build and Test

Docker container with cmake.

Tests are automated with Cpputest.

## Debugging in McuExpresso with LPC-Link2 probe

### Sbl component

Import the memory map build/app/SblRelease.xml

#### SblDebug 

Built with newlib (semihost-nf)

PRINT_ENABLE=1 defined for printing. Print functions hosted.

#### SblRelease

Built with newlib (nohost). No print support.

### App component

Import the memory map build/app/AppARelease.xml

#### AppADebug

Built with newlib (semihost-nf)

PRINT_ENABLE=1 defined for printing. Print functions hosted.

#### AppARelease

Built with newlib (nohost). No print support.

## Generating images

There are 2 types of images that are typically generated.

1. For ISP, a full image with SBL and APP components is generated. To achieve safe firmware upgrades, the flash holds two image banks or slots. At least one is for a stable image and another is for a candidate image. A full image contains the SBL in the lower 5kB of flash and the application in the following 13kB of flash. 

2. For IAP, only application images are generated. Two identical images are created for storing in either bank A or bank B depending on where the active (stable) image is already sotred in flash. The firmware upgrade client coordinates and decides which flash bank to download candidate images to.

Stable image is a production image with the SBL and APP in bank A.

Candidate image is an upgradable image with one version containing APP in bank A and another image file with the APP in bank B.

The image dir contains the images ready for flashing.

**buildimg.py** will generate images if on the **in_sys_rel** branch or the **in_app_rel** and there is a valid version tag matching the current commit.

## Flash layout

| Name  | @start  | @end   | byte size | sector size | 
|-------|---------|--------|-----------|-------------|
| SBL   |  0x0000 | 0x1400 |  5120 B   |  5          |
| App1  |  0x1400 | 0x33c0 | 13248 B   |  ~13        |
| App1Hdr  |  0x33c0 | 0x3400 | 64 B   |  1 page     |
| App2  |  0x4800 | 0x7BC0 | 13248 B   |  ~13        |
| App2Hdr  |  0x7BC0 | 0x7C00 | 64 B   |  1 page     |
| Data  |  0x7c00 | 0x7f7f | 896B      |  14 pages   |

## Delivery for production

The first stage bootloader (ROM loader) exposes a UART interface for ISP. A firmware image is downloaded to the microcontroller using the LPC21isp tool. See Ref.


## GPIO interface

### Debug, reset and ISP
SWDIO, SDWCLK, RESET, ISP_RXD, ISP_TXD

Firmware uploaded to uC via ISP_RXD, ISP_TXD.

Debugging done with LPC-Link2 Probe.


### Interface to Ls1088 SoC
Communication between SoC and uC is via i2c.

PIO0_28 (12) LPC_SDA
PIO0_29 (11) LPC_SCK

### Core functionality
Control the power up of the main processor.



### Reset Configuration Word (RCW) selection

#### RCW_SRC_SEL
PIO0_1 (17) configured as an output.
Drive low into nand gate (U12 74LVC1G14) which sets the two QSPI_RCW_SEL lines high in the LS1088 cfg_rcw_src port,
changing the the RCW from uSDHC to qSPI.

#### RCW_SEL_SW 
PIO0_18 (31) configured as an input (with pullup ?).
This input comes from the dip switch selector (SW2).

### SoC power down

#### SYS_POWER_DN
PIO0_10 (9) drives the gate of mosfet Q9, which intern drives the IFC_CLK0 on the LS1088.
Configure as output and keep low, turning the mosfet off and the IFC_CLK0 high.
To (power off ? the SoC), drive high, turning the mosfet on and the IFC_CLK0 low.

### SoC reset

#### PORESET
PIO0_17 (1)
Configure as output.
TODO: Confirm it goes to three different destinations.

#### RESET_REQ_IGNORE
PIO0_19 (26)
Configure as input.
Comes from dip switch selector (SW2). 
TODO: functionality ?

#### RESET_SW
PIO0_20 (15)
Configure as input.
Comes from output of U11 AND gate.
Active low reset switch from front panel ? See P1 connector.

#### RESET_REQ_B_33
PIO0_30 (21).
Input. 
Comes via 1.8 to 3.3 V level translator and from RESET_REQ_B from Ls1088 SoC.


### ATX power

#### ATX_PWR_OK
PIO0_13 (2).
Configure as input.
TODO: Confirm this is 5V from P2 if ATX plugged in.

#### ATX_PWR
PIO0_16 (32).
Configure as input.
This is 3v3 status of the 5V from the ATX pin 7 into U7 (5V to 3v3).


#### PWR_SW
PIO0_21 (10).
Configure as input.
Comes from the U15 AND gate.
Active low power switch from front panel? See P1 connector.

#### PS_ON
PIO0_22 (30).
Configure as output.
Drives mosfet Q1 which controls the ATX_PS_ON.

### Power good

#### 3V3_PGOOD
PIO0_7 (22).

#### 5V0_PGOOD
PIO0_9 (18).

#### CPU_VDD_PGOOD
PIO0_11 (8).

#### DDR_PGOOD
PIO0_23 (29).


### Thermal inputs

#### THERMAL_CRIT
PIO0_24 (13).
Input.

#### DDR_THERM_EVENT
PIO0_15 (16).
Input.


### Sleep

#### ASLEEP_3P3
PIO0_24 (28).
Configure as input.
Indicates when SoC is in sleep mode ?


### LED

#### PWR_GREEN_DRV
PIO0_8 (19).
Output which drives FET Q2 which is the green LED on the front panel ATX.

#### PLATFORM_FAIL
PIO0_25 (27).
Output to drive two LEDS (red and green).


### Misc

#### 10G_ENABLE
PIO0_26 (14).





## FSM C implementation

### STANDBY
5V from ATX powers the 5v/3v3 (U7 and U13) which generates 3V3_ALW. 3V3_ALW powers the uC.
In standby, the uC drives the PS_ON which turns on the 12V.
ATX_POWER_OK will be 5V if 12V is OK and transition to the POWER_APPLIED state.

Debug_gate is a software flag used to disable the transition. 

### POWER_APPLIED
Once 12V and VIN are applied to the PMIC, the state of 3V3_PGOOD and 5V0_PGOOD will be monitoried.
If both 3V3_PGOOD and 5V0_PGOOD are good, the CPU_VDD_EN will be asserted and the transition to CPU_VDD_ON state will occur.

### CPU_VDD_ON
If the CPU_VDD_PGOOD and the DDR_PGOOD are ok, the PORESET will be asserted.
The PORESET routine also configures the RCW according to the boot source which is selected by the dip switch (qSPI or eSDHC).
Boot source can be controlled by the dip switch and via the i2c api. If it is set via the i2c api, then it should only be a one shot.

### CPU_ON
Drive the LED_PLATFORM_FAIL according to the ASLEEP_3P3 input. 
Turn the GREEN_GREEN_DRV LED on.

Current code can go to the ATX_OFF state if a glitch in the power supply occurs which
is not desirable. Solution: If ATX_PWR_OK goes low here for x seconds, then go to ATX_OFF state. 

SoC can request a power off if the ASLEEP_3P3 goes low and the IGNORE_RESET_REQ is 0. It will go to ATX_OFF state.

SoC can also request a reset if RESET_REQ_B_33 is deasserted and IGNORE_RESET_REQ is 0. It will go to ATX_OFF state.

If the front panel ATX reset button is pressed for x (configurable via i2c api) seconds, the uC will initiate a PORESET.


### ATX_PWR_OFF
Set the state of the LED's. PLATFORM_FAIL to red and GREEN_DRV off.
Set CPU_VDD_EN off.
Deassert  the PORESET.
Deassert the ATX PS_ON control.
If the PWR_SW is low, then transition to the STANDBY state.



On uC power up, all inputs and outputs are configured.




