#include "fake_drivers.h"


void GPIO_PortInit(GPIO_Type *base, uint32_t port, uint32_t pin, const gpio_pin_config_t *config){}
void SYSCON_AttachSignal(SYSCON_Type *base, uint32_t index, syscon_connection_t connection){}
void PINT_Init(PINT_Type *base){}
void PINT_PinInterruptConfig(PINT_Type *base, pint_pin_int_t intr, pint_pin_enable_t enable, pint_cb_t callback){}
void PINT_EnableCallbackByIndex(PINT_Type *base, pint_pin_int_t pintIdx){}

uint32_t GPIO_PinRead(GPIO_Type *base, uint32_t port, uint32_t pin){
    for (int i=0;i<11;i++){
        if (pin == gpioInputs[i].pin){
            return gpioInputs[i].value;
        }
    }
}

void GPIO_PinWrite(GPIO_Type *base, uint32_t port, uint32_t pin, uint8_t output){
    for (int i=0;i<6;i++){
        if (pin == gpioOutputs[i].pin){
            gpioOutputs[i].value = output;
            //printf("Write pin %d value %d\n", pin, value);
            simulate();
        }
    }
}
