
#include "twi.h"
#include "printout.h"


static TwiPrivate_t twiPriv;
i2c_slave_handle_t g_s_handle;
void (*Twi_ProcessData)(const uint8_t *, uint8_t *, uint8_t *);



static void Twi_SlaveCallback(I2C_Type *base, volatile i2c_slave_transfer_t *xfer, void *userData)
{
	uint8_t recv_addr = 0;

    switch (xfer->event)
    {
        // Address match
        case kI2C_SlaveAddressMatchEvent:
            xfer->rxData = NULL;
            xfer->rxSize = 0;
            recv_addr = I2C_SlaveGetReceivedAddress(base, xfer);
            if ((recv_addr & 1) == 1)
            	twiPriv.direction = kI2C_Read;
            else if ((recv_addr & 1) == 0)
            	twiPriv.direction = kI2C_Write;
            break;

        /* uC send, uP recv: typically after a repeated start i.e. START ADDR(w) DATA START ADDR(r) DATA STOP */
        case kI2C_SlaveTransmitEvent:
        	//twiPriv.txLength = 0;
        	//Twi_ProcessData(twiPriv.rxBuff, twiPriv.txBuff, &twiPriv.txLength);
            xfer->txData = twiPriv.txBuff;
            xfer->txSize = twiPriv.txLength;
            break;

        /* uP send, uC recv: Could be first part of a write/read transaction or just a write transaction */
        case kI2C_SlaveReceiveEvent:
            xfer->rxData = twiPriv.rxBuff;
            xfer->rxSize = TWI_MAX_DATA_LEN;
            break;

        /* uC complete sending data to uP */
        case kI2C_SlaveCompletionEvent:
        	break;

        /* uP sent stop condition, if after a write then process the data */
        case kI2C_SlaveDeselectedEvent:
        	if (twiPriv.direction == kI2C_Write){
				twiPriv.txLength = 0;
				Twi_ProcessData(twiPriv.rxBuff, twiPriv.txBuff, &twiPriv.txLength);
        	}
            break;

        default:
            break;
    }


}

uint8_t Twi_Init(void (*onReady)(const uint8_t *, uint8_t *, uint8_t *))
{

	status_t retval;

	Twi_ProcessData = onReady;

	i2c_slave_config_t config;

	I2C_SlaveGetDefaultConfig(&config);
	config.address0.address = TWI_SLAVE_ADDRESS;
	config.busSpeed = kI2C_SlaveStandardMode;

	I2C_SlaveInit(TWI_BUS_CONTROLLER, &config, TWI_CLOCK_FREQ);

	// Create the I2C handle for the non-blocking transfer
	I2C_SlaveTransferCreateHandle(TWI_BUS_CONTROLLER, &g_s_handle, Twi_SlaveCallback, NULL);

	// Start accepting I2C transfers on the I2C slave peripheral
	retval = I2C_SlaveTransferNonBlocking(TWI_BUS_CONTROLLER, &g_s_handle, kI2C_SlaveAllEvents);

	if (retval != kStatus_Success)
	{
		TWI_PRINT("Error setting up I2C: %d", retval);
		return 1;
	}

	TWI_PRINT("Twi initialised\n");
	return 0;
}

/* Re-init after a power on reset */
uint8_t Twi_Reinit(void)
{

	status_t retval;

	i2c_slave_config_t config;

	I2C_SlaveGetDefaultConfig(&config);
	config.address0.address = TWI_SLAVE_ADDRESS;
	config.busSpeed = kI2C_SlaveStandardMode;

	I2C_SlaveDeinit(TWI_BUS_CONTROLLER);

	I2C_SlaveInit(TWI_BUS_CONTROLLER, &config, TWI_CLOCK_FREQ);

	// Create the I2C handle for the non-blocking transfer
	I2C_SlaveTransferCreateHandle(TWI_BUS_CONTROLLER, &g_s_handle, Twi_SlaveCallback, NULL);

	// Start accepting I2C transfers on the I2C slave peripheral
	retval = I2C_SlaveTransferNonBlocking(TWI_BUS_CONTROLLER, &g_s_handle, kI2C_SlaveAllEvents);

	if (retval != kStatus_Success)
	{
		TWI_PRINT("Error setting up I2C: %d", retval);
		return 1;
	}

	TWI_PRINT("Twi reinitialised\n");
	return 0;
}


