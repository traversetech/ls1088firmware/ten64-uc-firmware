/*
 * image.h
 *
 *  Created on: 16 Feb 2021
 *      Author: vaughn
 */

#ifndef IMAGE_H_
#define IMAGE_H_
#include <stdint.h>
#include "fwup.h"


void Image_SetAsStable(void);
uint8_t Image_Verify(const uint32_t * imgStartAddr, const FwupImageHeader_t * imgHeader);
void Image_SetBootCount(const uint8_t bootCount);
void Image_UpdateHeader(const FwupBankId_t bankId, const FwupImageType_t type);
void Image_LoadVerifyRun(void);

#endif /* IMAGE_H_ */
