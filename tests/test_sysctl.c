#include "test_sysctl.h"
#include "sysctl.h"
#include <unistd.h>



#define PIN_IN_VDD_PGOOD 					11U
#define PIN_IN_3V3_PGOOD 					7U
#define PIN_IN_5V0_PGOOD 					9U
#define PIN_IN_DDR_PGOOD 					23U
#define PIN_IN_ATX_PWR 						16U	// 5v0 into U7, stat output would be high (3v3 ref)
#define PIN_IN_ATX_PWR_OK					13U	// pgood out of P2, should assert after PS_ON driven to turn 12V atx on
#define PIN_IN_ASLEEP_3P3 					24U
#define PIN_IN_PWR_SW						21U
#define PIN_IN_RCW_SEL_SW 					18U
#define PIN_IN_RESET_REQ_IGNORE 			19U
#define PIN_IN_RESET_REQ_B33 				30U

#define PIN_OUT_PS_ON 						22U
#define PIN_OUT_PORESET 					17U
#define PIN_OUT_CPUVDD_EN 					14U
#define PIN_OUT_LED_PLATFORM_FAIL 			25U
#define PIN_OUT_LED_GREEN_DRV				8U
#define PIN_OUT_RCW_SRC_SEL 				1U

typedef struct {
	uint8_t pin;
	uint8_t value;
} TestGpio_t;



TestGpio_t gpioInputs[11] = {
		{PIN_IN_VDD_PGOOD, 0U},
		{PIN_IN_3V3_PGOOD, 0U},
		{PIN_IN_5V0_PGOOD, 0U},
		{PIN_IN_DDR_PGOOD, 0U},
		{PIN_IN_ATX_PWR, 0U},
		{PIN_IN_ATX_PWR_OK, 0U},
		{PIN_IN_ASLEEP_3P3, 1U},
		{PIN_IN_PWR_SW, 0U},
		{PIN_IN_RCW_SEL_SW, 0U},
		{PIN_IN_RESET_REQ_IGNORE, 0U},
		{PIN_IN_RESET_REQ_B33, 1U}
};

TestGpio_t gpioOutputs[6] = {
		{PIN_OUT_PS_ON, 0U},
		{PIN_OUT_PORESET, 0U},
		{PIN_OUT_CPUVDD_EN, 0U},
		{PIN_OUT_LED_PLATFORM_FAIL, 0U},
		{PIN_OUT_LED_GREEN_DRV, 0U},
		{PIN_OUT_RCW_SRC_SEL, 0U}
};

void Flash_StoreData(const uint32_t addr, const uint8_t * data, const uint16_t length){
	SYSCTL_PRINT("Store data\n");
}

uint8_t Flash_ReadData(uint32_t addr, uint8_t * data, uint16_t length){
	SYSCTL_PRINT("Retrieve data\n");
}
void SysTick_DelayTicks(uint32_t n) {sleep(1);}
void SysTick_ElapsedStart() {}
uint32_t SysTick_ElapsedRead() {return 0;}
void GPIO_PortInit(GPIO_Type *base, uint32_t port){}
void SYSCON_AttachSignal(SYSCON_Type *base, uint32_t index, syscon_connection_t connection){}
void PINT_Init(PINT_Type *base){}
void PINT_PinInterruptConfig(PINT_Type *base, pint_pin_int_t intr, pint_pin_enable_t enable, pint_cb_t callback){}
void PINT_EnableCallbackByIndex(PINT_Type *base, pint_pin_int_t pintIdx){}

static void simulate(void){
	for (int i=0;i<6;i++){

		// simulate ATX on,
		if (PIN_OUT_PS_ON == gpioOutputs[i].pin && gpioOutputs[i].value == 1){
			gpioInputs[4].value = 1; // atx power ok
			gpioInputs[1].value = 1; // 3v3 pgood
			gpioInputs[2].value = 1; // 5v0 pgood

		}

		// simualte vdd en
		if (PIN_OUT_CPUVDD_EN == gpioOutputs[i].pin && gpioOutputs[i].value == 1){
			gpioInputs[0].value = 1; // vdd pgood
			gpioInputs[3].value = 0; // ddr pgood
		}
	}
}

uint8_t GPIO_PinRead(uint8_t pin){
	for (int i=0;i<11;i++){
		if (pin == gpioInputs[i].pin){
			return gpioInputs[i].value;
		}
	}
}

void GPIO_PinWrite(uint8_t pin, uint8_t value){
	for (int i=0;i<6;i++){
		if (pin == gpioOutputs[i].pin){
			gpioOutputs[i].value = value;
			//printf("Write pin %d value %d\n", pin, value);
			simulate();
		}
	}
}

uint8_t TestTwiReinitCallback(void){
	printf("Test twi reinit called\n");
	return 0;
}


void main(void){

	SysCtl_Init(TestTwiReinitCallback);
	SysCtl_Run();

}
