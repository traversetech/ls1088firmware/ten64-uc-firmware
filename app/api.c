#include "api.h"
#include "bdinfo.h"
#ifndef UTEST
#include "fwup.h"
#include "sysctl.h"
#include "twi.h"
#endif
#include "printout.h"


static ApiCall_t apiCalls[API_COMMANDS_MAX] =
{
    { .cmd = apiBdInfoSetMac,               .apiCall = BdInfo_ApiSetMacAddr },
    { .cmd = apiBdInfoGetInfo,              .apiCall = BdInfo_ApiGetInfo },

    { .cmd = apiSysCtlGetState,             .apiCall = SysCtl_ApiGetSystemState },
    { .cmd = apiSysCtlSetResetHoldtime,     .apiCall = SysCtl_ApiSetResetHoldTime },
    { .cmd = apiSysCtlSetResetEnable,       .apiCall = SysCtl_ApiSetResetEnabled },
    { .cmd = apiSysCtlSetNextBootsource,    .apiCall = SysCtl_ApiSetNextBootSource },
    { .cmd = apiSysCtlSet10gEnable,         .apiCall = SysCtl_ApiSetEnable10g},

    { .cmd = apiFwupGetInfo,                .apiCall = Fwup_ApiGetInfo },
    { .cmd = apiFwupInit,                   .apiCall = Fwup_ApiInit },
    { .cmd = apiFwupXfer,                   .apiCall = Fwup_ApiTransfer },
    { .cmd = apiFwupCheck,                  .apiCall = Fwup_ApiCheck },
    { .cmd = apiFwupBoot,                   .apiCall = Fwup_ApiBoot}
};

void API_ProcessData(const uint8_t * rxData, uint8_t * txData, uint8_t * txLen)
{

	if (rxData == NULL || txData == NULL)
	{
		return;
	}


    ApiMessage_t * toSend = (ApiMessage_t *)txData;
    ApiMessage_t * recv = (ApiMessage_t *)rxData;

    // check preamb in recv packet
    if (recv->preamb != API_HEADER_PREAMB)
    {
        API_PRINT("No preamb in header\n");
        return;
    }

    *txLen = 0;
    toSend->cmd = recv->cmd;
    toSend->preamb = API_HEADER_PREAMB;

    // iterate through array to see if command supported
    for (int i=0; i<API_COMMANDS_MAX; i++)
    {
        if (apiCalls[i].cmd == recv->cmd && apiCalls[i].apiCall != NULL)
        {
            *txLen = apiCalls[i].apiCall(recv->data, toSend->data);
        }
    }

    toSend->len = *txLen;

    // if there is data to send, add API_HEADER_SIZE more for the cmd
	if (*txLen)
	{
        *txLen += API_HEADER_SIZE;
	}

    API_PRINT("Recv cmd=%d, ready to send %d B\n", recv->cmd, *txLen);
}
