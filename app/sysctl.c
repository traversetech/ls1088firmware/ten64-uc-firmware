#include "sysctl.h"
#include "timer.h"
#include "fsl_gpio.h"
#include "fsl_pint.h"
#include "fsl_syscon.h"
#include "printout.h"
#include "flash.h"

#define PIN_INIT(pin,config)   		GPIO_PinInit(GPIO, 0, pin, config);
#define PIN_READ(pin) 				GPIO_PinRead(GPIO, 0, pin)
#define PIN_WRITE(pin,value)		GPIO_PinWrite(GPIO, 0, pin, value);

static TwiReinit_t twiReinitCb = NULL;
static SysCtlPrivateState_t sysctlPriv;

static SysCtlInput_t inputs[inMax] =
{
    { .rollavg = 0U, .pin = PIN_IN_VDD_PGOOD, 		    .value = 0U },
    { .rollavg = 0U, .pin = PIN_IN_3V3_PGOOD, 		    .value = 0U },
    { .rollavg = 0U, .pin = PIN_IN_5V0_PGOOD, 		    .value = 0U },
    { .rollavg = 0U, .pin = PIN_IN_DDR_PGOOD, 		    .value = 1U }, // active low
    { .rollavg = 0U, .pin = PIN_IN_RESET_REQ_B33,       .value = 0U },
    { .rollavg = 0U, .pin = PIN_IN_RESET_REQ_IGNORE,    .value = 0U },
    { .rollavg = 0U, .pin = PIN_IN_RCW_SEL_SW,          .value = 0U },
    { .rollavg = 0U, .pin = PIN_IN_ASLEEP_3P3,          .value = 0U },
    { .rollavg = 0U, .pin = PIN_IN_ATX_PWR, 			.value = 0U }, // U7 pin 1 STAT, +5VSB from ATX
    { .rollavg = 0U, .pin = PIN_IN_ATX_PWR_OK,          .value = 0U }  // AT_PWR_OK from ATX header should be high after issueing PS_ON
};

/* @brief SysCtl_PinResetInterrupt
 * @details	ISR for pin interrupt.
 *
 * 			kPINT_PinInt0 is for reset on gpio 20
 * 			kPINT_PinInt1 is for power on gpio 21
 *
 * @param pintr	Pin interrupt number
 * @param pmatch_status not used
 */
void SysCtl_PinInterrupt(pint_pin_int_t pintr, uint32_t pmatch_status)
{
	uint32_t elapsed;

	SYSCTL_PRINT("Pin interrupt %d\n", pintr);

	// ext reset button trigger
	if (pintr == kPINT_PinInt0)
	{

		if (inputs[inIgnoreRst].value == lvlHigh)
		{
			SYSCTL_PRINT("Ignoring due to dip select\n");
		}
		// ignore if one of these two flags are disabled
		else if (sysctlPriv.resetEnabled == rstDisable)
		{
			SYSCTL_PRINT("Ignoring due to reset disabled\n");
		}
		// ignore if not in active state
		else if (sysctlPriv.currentState != stateActive)
		{
			SYSCTL_PRINT("Ignoring as not in active state\n");
		}
		// ext power button should not be pressed while ext reset button is being pressed
		else if (bsPress == sysctlPriv.extPowerState)
		{
			SYSCTL_PRINT("Ext power button is currently active, ignore ext reset button event\n");
		}
		// currently releases, go to pressed and start a countdown timer
		else if (bsRelease == sysctlPriv.extResetState)
		{
			sysctlPriv.extResetState = bsPress;
			SYSCTL_PRINT("Reset press\n");
			Timer_Start(tmrSysctl);
		}
		// currently pressed, go to released and check the countdown timer has reached 0 i.e. button held down for appropriate time
		else if (bsPress == sysctlPriv.extResetState)
		{
			sysctlPriv.extResetState = bsRelease;
			SYSCTL_PRINT("Reset release\n");
			elapsed = Timer_Elapsed(tmrSysctl);
			printf("Timer elapsed=%d\n", elapsed);
			if (elapsed >= sysctlPriv.resetHoldtime*1000)
			{
				sysctlPriv.extResetState = bsLongActivated;
			}
		}
	}
	// ext power button trigger
	else if (pintr == kPINT_PinInt1)
	{
		// must be in either active state or atx pwr idle state
		if (sysctlPriv.currentState != stateActive && sysctlPriv.currentState != stateAtxPwrIdle)
		{
			SYSCTL_PRINT("Ignoring as not in active state or atx pwr idle state\n");
		}
		// ext reset button should not be pressed while ext power button is being pressed
		else if (bsPress == sysctlPriv.extResetState)
		{
			SYSCTL_PRINT("Ext reset button is currently active, ignore ext power button event\n");
		}
		else if (bsRelease == sysctlPriv.extPowerState)
		{
			sysctlPriv.extPowerState = bsPress;
			SYSCTL_PRINT("Power press\n");
			Timer_Start(tmrSysctl);
		}
		else if (bsPress == sysctlPriv.extPowerState)
		{
			sysctlPriv.extPowerState = bsRelease;
			SYSCTL_PRINT("Power release\n");
			elapsed = Timer_Elapsed(tmrSysctl);

			// long press
			if (elapsed >= sysctlPriv.resetHoldtime*1000)
			{
				sysctlPriv.extPowerState = bsLongActivated;
			}
			// short press
			else if (elapsed >= 50)
			{
				sysctlPriv.extPowerState = bsShortActivated;
			}
		}
	}
}

/* @brief SysCtl_StoreData
 * @details	Store private variables to flash.
 */
static void SysCtl_StoreData(void)
{
	SysCtlDataStore_t dataStore;
	dataStore.resetEnable = sysctlPriv.resetEnabled;
	dataStore.resetHoldtime = sysctlPriv.resetHoldtime;
	Flash_StoreData(SYSCTL_DATA_STORE_IN_FLASH, (uint8_t *)&dataStore, sizeof(SysCtlDataStore_t));
}


/* @brief SysCtl_RetrieveData
 * @details	Set private variables from flash if they are valid.
 */
static void SysCtl_RetrieveData(void)
{
	SysCtlDataStore_t dataStore;

	// set defaults
	sysctlPriv.nextBootSourceFromApi = bootSrcUnknown;
    sysctlPriv.resetEnabled = rstEnable;
	sysctlPriv.resetHoldtime = RESET_HOLDTIME_DEFAULT;

	if ( feSuccess == Flash_ReadData(SYSCTL_DATA_STORE_IN_FLASH, (uint8_t *)&dataStore, sizeof(SysCtlDataStore_t)) )
	{
        if (dataStore.resetEnable == rstEnable || dataStore.resetEnable == rstDisable)
        {
			sysctlPriv.resetEnabled = dataStore.resetEnable;
        }

		if (dataStore.resetHoldtime >= 3U && dataStore.resetHoldtime <10U)
		{
			sysctlPriv.resetHoldtime = dataStore.resetHoldtime;
		}

	}
}

/* @brief SysCtl_CheckBootSource
 * @details	Detect next boot source either if set by sw api (if valid) or by the dip switch.
 */
static uint8_t SysCtl_CheckBootSource(void)
{
	uint8_t retval;

	// use api selected boot source if it is valid
	if (sysctlPriv.nextBootSourceFromApi == bootSrcNand || sysctlPriv.nextBootSourceFromApi == bootSrcSdcard)
	{
		retval = sysctlPriv.nextBootSourceFromApi;
	}
	// else use boot dip switch selected boot source
	else
	{
		retval = inputs[inBootSel].value;
	}
	return retval;
}

/* @brief SysCtl_DoPoreset
 * @details		Sets the next boot source by writing to RCW pin out and then performs a POR.
 */
static void SysCtl_DoPoreset(void) {

	sysctlPriv.nextBootSource = SysCtl_CheckBootSource();

	sysctlPriv.currentBootSource = sysctlPriv.nextBootSource;

	// reset next boot source from api as this should only be a 1-shot
	sysctlPriv.nextBootSourceFromApi = bootSrcUnknown;
	sysctlPriv.nextBootSource = inputs[inBootSel].value;

	if (sysctlPriv.currentBootSource == bootSrcNand)
	{
		SYSCTL_PRINT("Boot source is NAND\n");
	}
	else if (sysctlPriv.currentBootSource == bootSrcSdcard)
	{
		SYSCTL_PRINT("Boot source is SDCARD\n");
	}

	PIN_WRITE(PIN_OUT_RCW_SRC_SEL, sysctlPriv.currentBootSource);

	Timer_DelayMs(200U);

	PIN_WRITE(PIN_OUT_PORESET, lvlLow);
	Timer_DelayMs(500U);
	PIN_WRITE(PIN_OUT_PORESET, lvlHigh);
	Timer_DelayMs(500U);

	// re-initialise the twi endpoint
	if (twiReinitCb != NULL)
	{
		twiReinitCb();
	}
}

/**
 * @brief SysCtl_ProcessInput
 * @details                     Buffers 8 samples in rollavg. If at least 5 samples
                                are 1 then set value to 1 else set value to 0
 */
static void SysCtl_ProcessInput(SysCtlInput_t * input, uint8_t count)
{
	uint8_t pinState = PIN_READ(input->pin);
	input->rollavg += pinState;

	//SYSCTL_PRINT("Input pin %d: Pin value = %d (rolling avg = %02X)\n", input->pin, input->value, input->rollavg);
	if (count >= 7)
	{
		if (input->rollavg >= 5)
		{
			input->value = lvlHigh;
		}
		else
		{
			input->value = lvlLow;
        }

		input->rollavg = 0U;
	}
}

/**
 * @brief SysCtl_SampleInputs
 * @details                     Sample all inputs over ~8ms.
 */
static void SysCtl_SampleInputs(void){
	uint32_t inputCounter = 0;
	while (inputCounter < 8)
	{
		for (int i=0; i<inMax; i++)
		{
			SysCtl_ProcessInput(&inputs[i], inputCounter);
		}
        inputCounter++;
		Timer_DelayMs(1U);
	}

#if 0
	for (int i=0;i<inMax;i++)
	{
		SYSCTL_PRINT("Input pin %d: Pin value = %d\n", inputs[i].pin, inputs[i].value);
	}
#endif
}

/**
 * @brief SysCtl_IoOff
 * @details                     Set LED states accordingly, pull Vdden low.
 */
static void SysCtl_IoOff(void)
{
	// leds
	PIN_WRITE(PIN_OUT_LED_PLATFORM_FAIL, lvlLow);
	PIN_WRITE(PIN_OUT_LED_GREEN_DRV, lvlHigh);

	// disable vdd on pmic
	PIN_WRITE(PIN_OUT_CPUVDD_EN, lvlLow);

	// disable 12v atx
	PIN_WRITE(PIN_OUT_PS_ON, lvlLow);

	// deassert por
	PIN_WRITE(PIN_OUT_PORESET, lvlLow);
}

/**
 * @brief SysCtl_Run
 * @details Main state machine for sampling gpio inputs, state transitions and controlling outputs.
 */
void SysCtl_Run(void)
{

		// sample and average inputs over 8 readings (~8ms) to avoid spurious readings
		SysCtl_SampleInputs();

		switch (sysctlPriv.currentState)
		{
			// Atx 5V is present, wait for ext pwr on button to be pressed
			case stateAtxPwrIdle:

				if (bsShortActivated == sysctlPriv.extPowerState || bsLongActivated == sysctlPriv.extPowerState)
				{
					SYSCTL_PRINT("Power on triggered from ext button\n");
					sysctlPriv.extPowerState = bsRelease;
					sysctlPriv.currentState = stateAtxReady;
				}

				break;

			// Ext atx power button pressed, assert PS_ON to enable 12V
			case stateAtxReady:
				// ATX power input detected and power applied, go to state statePowerApplied
				if (lvlHigh == inputs[inAtxPwrOk].value)
				{
					SYSCTL_PRINT("ATX 12V ready\n");
					sysctlPriv.currentState = statePowerApplied;
				}
				// ATX power input detected, now assert PS_ON to turn the output on, wait for ATX_PWR_OK to transition
				else
				{
					SYSCTL_PRINT("Issuing ATX_PWR_ON\n");
					PIN_WRITE(PIN_OUT_PS_ON, lvlHigh);
                    Timer_DelayMs(1000U);
				}
				break;

			// 12V is present, wait for 5V and 3V3 rails to come up
			case statePowerApplied:
				// Once 3v3 and 5v0 are stable, assert cpu vdd enable and go to stateVddOn
				if (lvlHigh == inputs[in3v3Pgood].value && lvlHigh == inputs[in5v0Pgood].value)
				{
					SYSCTL_PRINT("Powering on CPUVDD\n");
					PIN_WRITE(PIN_OUT_CPUVDD_EN, lvlHigh);
					sysctlPriv.currentState = stateVddOn;
                    Timer_DelayMs(40U);
				}
				break;

			// Wait for VDD pgood and DDR pgood before issueing POR
			case stateVddOn:
				// Once CPU_VDD_PGOOD and DDR_PGOOD are stable, do power on reset and go to stateCpuOn
				if (lvlHigh == inputs[inVddPgood].value && lvlLow == inputs[inDdrPgood].value)
				{
					SYSCTL_PRINT("VDD and DDR are good, issuing PORESET\n");
					SysCtl_DoPoreset();
					sysctlPriv.currentState = stateActive;
                    Timer_DelayMs(500U);
				}
				break;

			// All systems up and running
			case stateActive:
				//SYSCTL_PRINT("System running\n");

				PIN_WRITE(PIN_OUT_LED_PLATFORM_FAIL, inputs[inAsleep].value);
				PIN_WRITE(PIN_OUT_LED_GREEN_DRV, lvlLow);

                /* ISSUE: Short loss of power from ATX (12V) will cause the ATX_PWR_OK,
                  5V0_PGOOD and 3V3_PGOOD inputs to go low. However, the low current consumption by the uC
                  allows it to continue running during the "power blip". Once the power returns, the uP needs to
                  be re-initialised.

                  FIX: Detect the above condition and jump back into atx ready state and attempt to re-initialise
                */

                if (lvlLow == inputs[in3v3Pgood].value && lvlLow == inputs[in5v0Pgood].value)
                {
                    SYSCTL_PRINT("Unexpected power loss detected, moving to stateAtxReady\n");
                    sysctlPriv.currentState = stateAtxReady;
                    SysCtl_IoOff();
                    Timer_DelayMs(3000U);
                }

                // Power off from uP via the ASLEEP input. If atx, go to atx pwr idle else go to shutdown
                else if (lvlLow == inputs[inAsleep].value)
                {
                	SYSCTL_PRINT("Power off request from uP\n");
                    SysCtl_IoOff();
                    sysctlPriv.currentState = stateAtxPwrIdle;
                }

                // Reset request from uP. Go back to statePowerApplied
                else if (lvlLow == inputs[inRstB33].value)
                {
					if (lvlLow == inputs[inIgnoreRst].value)
					{
						SYSCTL_PRINT("Reset requested from board\n");
						sysctlPriv.currentState = statePowerApplied;
					}
					else
					{
						SYSCTL_PRINT("Reset ignored due to switch setting\n");
					}
                }

                // Reset from ext reset button. Go back to vddon state and do POR
                else if (bsLongActivated == sysctlPriv.extResetState)
                {
					SYSCTL_PRINT("Reset triggered from ext button\n");
					sysctlPriv.extResetState = bsRelease;
					sysctlPriv.currentState = stateVddOn;
                }

                // Ext power off button (long press). Go to atx power idle state where we wait for ext power on button press to activate again.
                else if (bsLongActivated == sysctlPriv.extPowerState)
                {
                	SYSCTL_PRINT("Power off (long press) triggered from ext button\n");
					sysctlPriv.extPowerState = bsRelease;
					sysctlPriv.currentState = stateAtxPwrIdle;
					SysCtl_IoOff();
                }

                // Ext power off button (short press). Assert the SYS_POWER_DN to the uP. uP will gracefully shutdown and assert ASLEEP.
                else if (bsShortActivated == sysctlPriv.extPowerState)
                {
                	SYSCTL_PRINT("Power off (short press) triggered from ext button\n");
                	sysctlPriv.extPowerState = bsRelease;
                    // need to toggle here ~500ms
                    PIN_WRITE(PIN_OUT_SYS_POWER_DN, lvlHigh);
                	Timer_DelayMs(500U);
                    PIN_WRITE(PIN_OUT_SYS_POWER_DN, lvlLow);
                }

				break;

			// Only way to get here is if NOT powered by ATX and uP has issued a graceful shutdown
			// NOTE: Only way out of this state is to pull 12V power out and re-apply
			case stateShutdown:
				SysCtl_IoOff();
				break;

			default:
                Timer_DelayMs(1000U);
				break;
		}
}

/**
 * @brief SysCtl_Init
 * @details                     Initialise all gpios and set default state variables.
 * @param twiReinitCallback     A callback to re-initialise the i2c interface. This is by design at every POR call.
 */
void SysCtl_Init(TwiReinit_t twiReinitCallback)
{
	twiReinitCb = twiReinitCallback;

	sysctlPriv.currentState = statePowerApplied;
	sysctlPriv.extResetState = bsRelease;
	sysctlPriv.extPowerState = bsRelease;

	// restore data from flash
	SysCtl_RetrieveData();

#ifndef TARGET_DEVKIT

	gpio_pin_config_t digital_output_default_0_config =
	{
		kGPIO_DigitalOutput, 0,
	};

    gpio_pin_config_t digital_input_config =
    {
    	kGPIO_DigitalInput, 0
    };

	// Init output LED GPIO.
    GPIO_PortInit(GPIO, 0);

    // Connect trigger sources to PINT for ext buttons RESET (gpio20) and POWER (gpio21)
    SYSCON_AttachSignal(SYSCON, kPINT_PinInt0, PIN_IN_RESET_SW);
    SYSCON_AttachSignal(SYSCON, kPINT_PinInt1, PIN_IN_PWR_SW);

	PINT_Init(PINT);

	// configure RESET on PinInt0, events on both rising and falling edges
	PINT_PinInterruptConfig(PINT, kPINT_PinInt0, kPINT_PinIntEnableBothEdges, SysCtl_PinInterrupt);
	PINT_EnableCallbackByIndex(PINT, kPINT_PinInt0);

	// configure POWER on PinInt1, events on both rising and falling edges
	PINT_PinInterruptConfig(PINT, kPINT_PinInt1, kPINT_PinIntEnableBothEdges, SysCtl_PinInterrupt);
	PINT_EnableCallbackByIndex(PINT, kPINT_PinInt1);

    PIN_INIT(PIN_OUT_LED_PLATFORM_FAIL, &digital_output_default_0_config);

    PIN_INIT(PIN_IN_3V3_PGOOD, &digital_input_config);
    PIN_INIT(PIN_IN_5V0_PGOOD, &digital_input_config);
    PIN_INIT(PIN_IN_VDD_PGOOD, &digital_input_config);
    PIN_INIT(PIN_IN_DDR_PGOOD, &digital_input_config);

    // SYS_POWER_DN on gpio 10
    PIN_INIT(PIN_OUT_SYS_POWER_DN, &digital_output_default_0_config);
    PIN_WRITE(PIN_OUT_SYS_POWER_DN, lvlLow);

    // 10G_ENABLE on gpio 26
    PIN_INIT(PIN_OUT_10G_ENABLE, &digital_output_default_0_config);
    PIN_WRITE(PIN_OUT_10G_ENABLE, lvlLow);

	// RCW SEL sel (note - this goes through an inverter
	PIN_INIT(PIN_OUT_RCW_SRC_SEL, &digital_output_default_0_config);

	// PORESET
	PIN_INIT(PIN_OUT_PORESET, &digital_output_default_0_config);
	PIN_WRITE(PIN_OUT_PORESET, lvlLow);

	PIN_INIT(PIN_OUT_CPUVDD_EN, &digital_output_default_0_config);

	// ASLEEP
	PIN_INIT(PIN_IN_ASLEEP_3P3, &digital_input_config);

	// RCW sel switch (SD<->QSPI)
	PIN_INIT(PIN_IN_RCW_SEL_SW, &digital_input_config);

	// IGNORE_RESET_REQ
	PIN_INIT(PIN_IN_RESET_REQ_IGNORE, &digital_input_config);

	// ATX Power Supply control
	PIN_INIT(PIN_IN_ATX_PWR, &digital_input_config);
	PIN_INIT(PIN_IN_ATX_PWR_OK, &digital_input_config);
	PIN_INIT(PIN_OUT_PS_ON, &digital_output_default_0_config);

	PIN_INIT(PIN_OUT_LED_GREEN_DRV, &digital_output_default_0_config);
	PIN_WRITE(PIN_OUT_LED_GREEN_DRV, lvlHigh);

    PIN_WRITE(PIN_OUT_LED_PLATFORM_FAIL, lvlLow);

    SysCtl_SampleInputs();

    // set the current boot source
    sysctlPriv.currentBootSource = inputs[inBootSel].value;

    // if 5v from atx is present, go to atx power idle and wait for ext pwr on
    // NOTE: ATX_PWR (gpio_16) is open drain output, from STAT of U7 (Power mux IC)
    // When board is powered by ATX i.e. U7 IN2, STAT is open drain (Hi-Z) but pulled high by R54.
    // When ATX 12V is up, U7 IN1 is active and STAT pulls low
    if (lvlHigh == inputs[inAtxPwr].value)
    {
        SYSCTL_PRINT("ATX 5V is present, going to atx ready\n");
        sysctlPriv.currentState = stateAtxReady;
    }

#endif
}


/**
 * @brief SysCtl_ApiGetSystemState
 * @param recv      Receive data
 * @param toSend    Data to send
 * @return          Size of the data (bytes) to return
 */
uint8_t SysCtl_ApiGetSystemState(const uint8_t * recv, uint8_t * toSend)
{

	ApiGetSystemState_t * systemState = (ApiGetSystemState_t *)toSend;
	systemState->atxPowerState = inputs[inAtxPwr].value;
	systemState->currentBootSource = sysctlPriv.currentBootSource;
	systemState->nextBootSource = sysctlPriv.nextBootSource;
	systemState->switchBootSource = inputs[inBootSel].value;
    systemState->resetEnabled = (sysctlPriv.resetEnabled == rstEnable)? 1:0;
	systemState->resetHoldtime = sysctlPriv.resetHoldtime;
	systemState->switchResetIgnore = inputs[inIgnoreRst].value;

	SYSCTL_PRINT("Api get system state\n");

	return sizeof(ApiGetSystemState_t);
}

/**
 * @brief SysCtl_ApiSetResetHoldTime
 * @param recv      Receive data
 * @param toSend    Data to send
 * @return          Size of the data (bytes) to return
 */
uint8_t SysCtl_ApiSetResetHoldTime(const uint8_t * recv, uint8_t * toSend)
{

	ApiSetResetHoldtime_t * setHoldtime = (ApiSetResetHoldtime_t *)recv;

	if (setHoldtime->holdtimeSeconds < 3U || setHoldtime->holdtimeSeconds >10U)
	{
		SYSCTL_PRINT("Api set reset holdtime %d invalid\n", setHoldtime->holdtimeSeconds);
    }
	else
    {
        sysctlPriv.resetHoldtime = setHoldtime->holdtimeSeconds;
        SysCtl_StoreData();
        SYSCTL_PRINT("Api set reset holdtime to %d secs\n", sysctlPriv.resetHoldtime);
    }

	return 0;
}

/**
 * @brief SysCtl_ApiSetResetEnabled
 * @param recv      Receive data
 * @param toSend    Data to send
 * @return          Size of the data (bytes) to return
 */
uint8_t SysCtl_ApiSetResetEnabled(const uint8_t * recv, uint8_t * toSend)
{

	ApiSetResetEnabled_t * resetEnable = (ApiSetResetEnabled_t *)recv;

    if (resetEnable->enable != 1U && resetEnable->enable != 0U)
    {
		SYSCTL_PRINT("Api set reset enable %d invalid\n", resetEnable->enable);
    }
    else
    {
        // transform 0 - > rstDisable and 1 -> rstEnable
        sysctlPriv.resetEnabled = (resetEnable->enable)? rstEnable : rstDisable;
        SysCtl_StoreData();
        SYSCTL_PRINT("Api set reset enable to %d\n", sysctlPriv.resetEnabled);
    }

	return 0;
}

/**
 * @brief SysCtl_ApiSetNextBootSource
 * @param recv      Receive data
 * @param toSend    Data to send
 * @return          Size of the data (bytes) to return
 */
uint8_t SysCtl_ApiSetNextBootSource(const uint8_t * recv, uint8_t * toSend)
{

	ApiSetNextBootSource_t * nextBootSource = (ApiSetNextBootSource_t *)recv;

	if (nextBootSource->nextBootSource >= bootSrcUnknown)
	{
		SYSCTL_PRINT("Api set next boot source %d invalid\n", nextBootSource->nextBootSource);
    }
	else
    {
        sysctlPriv.nextBootSource = sysctlPriv.nextBootSourceFromApi = nextBootSource->nextBootSource;
        SYSCTL_PRINT("Api set next boot source %d\n", sysctlPriv.nextBootSource);
    }

	return 0;
}

/**
 * @brief SysCtl_ApiSetEnable10g
 * @param recv      Receive data
 * @param toSend    Data to send
 * @return          Size of the data (bytes) to return
 */
uint8_t SysCtl_ApiSetEnable10g(const uint8_t *recv, uint8_t *toSend)
{
    ApiSet10gEnable_t * enable10g = (ApiSet10gEnable_t *)recv;

    if (ioSet == enable10g->control)
    {
        SYSCTL_PRINT("Api set 10g enable\n");
        PIN_WRITE(PIN_OUT_10G_ENABLE, lvlHigh);
    }
    else if (ioClear == enable10g->control)
    {
    	SYSCTL_PRINT("Api set 10g disable\n");
    	PIN_WRITE(PIN_OUT_10G_ENABLE, lvlLow);
    }
    else
    {
    	SYSCTL_PRINT("Api set 10g control %d invalid\n", enable10g->control);
    }

    return 0;
}
