/*
 * sbl.c
 *
 *  Created on: 17 Jun 2020
 *      Author: vaughn
 */
#include "image.h"

/**
 * Secondary bootloader main function
 * Checks which application to load, verifies the image
 * and runs
 */

//extern unsigned int __base_App1;



void main(void) {

	Image_LoadVerifyRun();

	// shouldn't reach here but if do go to infinate loop
	while (1) {}
}
