/*
 * twibus.h
 *
 *  Created on: 29 May 2020
 *      Author: vaughn
 */

#ifndef TWI_H_
#define TWI_H_

#include "fsl_gpio.h"
#include "fsl_i2c.h"
#include "pin_mux.h"

#define TWI_CLOCK_FREQ 				(12000000)
#define TWI_MAX_DATA_LEN 			(256) /* MAX is 256 */
#define TWI_BUS_CONTROLLER 			(I2C_Type *)(I2C0_BASE)
#ifdef TARGET_DEVKIT
#define TWI_SLAVE_ADDRESS			0x54
#else
#define TWI_SLAVE_ADDRESS			0x7E
#endif

typedef struct {
	i2c_direction_t direction;
	uint8_t txBuff[TWI_MAX_DATA_LEN];
	uint8_t rxBuff[TWI_MAX_DATA_LEN];
	uint8_t rxLength;
	uint8_t txLength;
	uint8_t bytesReceived;
	uint8_t bytesSent;
} TwiPrivate_t;

uint8_t Twi_Init(void (*onReady)(const uint8_t *, uint8_t *, uint8_t *));
uint8_t Twi_Reinit(void);

#endif /* TWI_H_ */
