/*
 * crc32.h
 *
 *  Created on: 16 Feb 2021
 *      Author: vaughn
 */

#ifndef CRC32_H_
#define CRC32_H_

#include <stdint.h>

uint32_t crc32(const void *buf, uint32_t size);

#endif /* CRC32_H_ */
