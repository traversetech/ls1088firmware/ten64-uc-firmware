/*
 * bdinfo.h
 *
 *  Created on: 13 Feb 2021
 *      Author: vaughn
 */

#ifndef BDINFO_H_
#define BDINFO_H_

#include "fwup.h"
#include <stdint.h>
#include "flash.h"

#define BDINFO_MAC_LOC_IN_FLASH					0x00007E00 // start of page 504

#define FW_MAJ									1U
#define FW_MIN									0U
#define FW_PTC  								0U

#pragma pack(1)
typedef struct {
	uint8_t mac[6];
} BdInfoMac_t;

typedef struct {
	uint8_t mac[6];
	uint32_t cpuId[4];
	FwupVersion_t fwVersion;
} BdInfoInfo_t;
#pragma pack()

/* Api methods exposed over i2c endpoint */
uint8_t BdInfo_ApiSetMacAddr(const uint8_t * recv, uint8_t * toSend);
uint8_t BdInfo_ApiGetInfo(const uint8_t * recv, uint8_t * toSend);

#endif /* BDINFO_H_ */
