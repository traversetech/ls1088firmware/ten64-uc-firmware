#!/bin/sh
rm -rf release CMakeFiles
rm -rf Makefile cmake_install.cmake CMakeCache.txt
cmake -DCMAKE_TOOLCHAIN_FILE="../cmake_toolchain_files/armgcc.cmake" -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=release  .
make -j$(nproc)
OBJCOPY_PATH="${ARMGCC_DIR}/bin/arm-none-eabi-objcopy"
${OBJCOPY_PATH} --pad-to=0x7BC0 --gap-fill=0xFF -O binary ./release/lpc804app.elf ./release/lpcapp_bankb.bin
