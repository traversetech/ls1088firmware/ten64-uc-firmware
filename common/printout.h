/*
 * printout.h
 *
 *  Created on: 26 Jun 2020
 *      Author: vaughn
 */

#ifndef PRINTOUT_H_
#define PRINTOUT_H_

//#define PRINT_ENABLE
#ifdef PRINT_ENABLE
#include <stdio.h>
#endif

#define API_PRINT_ENABLE
#define TWI_PRINT_ENABLE
#define FLASH_PRINT_ENABLE
#define MAIN_PRINT_ENABLE
#define SYSCTL_PRINT_ENABLE
#define FWUP_PRINT_ENABLE
#define BDINFO_PRINT_ENABLE
#define IMAGE_PRINT_ENABLE

#if defined(MAIN_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define MAIN_PRINT	printf
#else
#define MAIN_PRINT
#endif

#if defined(API_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define API_PRINT			printf
#else
#define API_PRINT
#endif

#if defined(FLASH_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define FLASH_PRINT			printf
#else
#define FLASH_PRINT
#endif

#if defined(SYSCTL_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define SYSCTL_PRINT			printf
#else
#define SYSCTL_PRINT
#endif

#if defined(TWI_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define TWI_PRINT			printf
#else
#define TWI_PRINT
#endif

#if defined(FWUP_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define FWUP_PRINT			printf
#else
#define FWUP_PRINT
#endif

#if defined(BDINFO_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define BDINFO_PRINT			printf
#else
#define BDINFO_PRINT
#endif

#if defined(IMAGE_PRINT_ENABLE) && defined(PRINT_ENABLE)
#define IMAGE_PRINT				printf
#else
#define IMAGE_PRINT
#endif

#endif /* PRINTOUT_H_ */
