/*
 * fwup.h
 *
 *  Created on: 13 Feb 2021
 *      Author: vaughn
 */

#ifndef FWUP_H_
#define FWUP_H_

#include <stdint.h>

#define FWUP_APP1_LOCATION_IN_FLASH              0x00001400
#define FWUP_APP2_LOCATION_IN_FLASH              0x00004800
#define FWUP_APP1_HDR_LOCATION_IN_FLASH          0x000047C0
#define FWUP_APP2_HDR_LOCATION_IN_FLASH          0x00007BC0

#define FWUP_APP_BANK_SIZE_SECTORS				13

#define FWUP_IMAGE_MAGIC						0xBEAFCAFE
#define FWUP_BOOT_IMAGE_DATA_IN_FLASH			0x00007C00
#define FWUP_HEADER_PAD_SIZE                    42U
#define FWUP_XFER_BLOCK_SIZE                    64U
#define FWUP_WRITE_BLOCK_SIZE                   256U

typedef enum {
    stateIdle = 0U,
    stateInit,
    stateXfer,
    stateCheck,
} FwupState_t;

typedef enum {
	bankId1 = 1U,
	bankId2 = 2U
} FwupBankId_t;

typedef enum {
	fwupSuccess = 0U,
	fwupErrBankIdInvalid,
	fwupErrGetInfoInvalid,
    fwupErrEraseSector,
    fwupErrBlankCheckSector,
    fwupErrWrite,
    fwupErrCompare,
    fwupErrCheckNoMagic,
    fwupErrCheckCrcFailed,
    fwupErrEraseStable,
	fwupErrUnknown
} FwupApiError_t;

typedef enum {
	imgVerifySuccess = 0U,
	imgVerifyErrMagic,
    imgVerifyErrCrc,
    imgVerifyNoImage
} FwupImageVerifyError_t;

typedef enum {
	imgInBankA = 1U,
	imgInBankB = 2U
} FwupImageLocation_t;

typedef enum {
	imgTypeStable = 1U,
	imgTypeCandidate,
	imgTypeUnstable,
	imgTypeOldStable,
	imgTypeForceOldStable,
	imgTypeClear
} FwupImageType_t;

typedef struct __attribute__((packed)) {
	uint32_t preamb;
	uint8_t bootCount;
    uint8_t bankId;
} FwupBootImageData_t;

typedef struct __attribute__((packed)) {
    uint32_t magic;
    uint32_t crc;
    uint16_t imageLength;
    uint8_t imageType;
    uint8_t versionMajor;
    uint8_t versionMinor;
    uint8_t versionPatch;
    char gitShortSHA[8];
    uint8_t pad[FWUP_HEADER_PAD_SIZE];
} FwupImageHeader_t;

typedef struct __attribute__((packed)) {
   FwupImageHeader_t bankAImg;
   FwupImageHeader_t bankBImg;
} FwupImageInfo_t;

typedef struct __attribute__((packed)){
    uint8_t state;
    uint8_t error;
} FwupRunState_t;

typedef struct __attribute__((packed)){
    uint8_t error;
    uint32_t addr;
    uint8_t currentBlock;
} FwupTransferResult_t;

typedef struct __attribute__((packed)){
	uint8_t maj;
	uint8_t min;
	uint8_t patch;
} FwupVersion_t;

typedef struct {
    FwupImageLocation_t location;
    uint8_t totalBlocksReceived;
    uint8_t totalBlocksFlashed;
} FwupPrivate_t;

/* Firmware upgrade api methods exposed over i2c endpoint */
uint8_t Fwup_ApiGetInfo(const uint8_t * recv, uint8_t * toSend);
uint8_t Fwup_ApiInit(const uint8_t * recv, uint8_t * toSend);
uint8_t Fwup_ApiTransfer(const uint8_t * recv, uint8_t * toSend);
uint8_t Fwup_ApiCheck(const uint8_t * recv, uint8_t * toSend);
uint8_t Fwup_ApiBoot(const uint8_t * recv, uint8_t * toSend);

#endif /* FWUP_H_ */
