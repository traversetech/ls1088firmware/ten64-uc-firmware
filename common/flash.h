#ifndef Flash_H_
#define Flash_H_

#include <stdint.h>

/* flash sections	(32 sectors (1024B/sector,64B/page, 16pages/sector) = 512 pages)
 * sbl 5120 B 		0x0000 - 0x13ff (5 sectors = 80 pages)
 * app1 13312 B		0x1400 - 0x47ff (13 sectors = 208 pages)
 * app2 13312 B		0x4800 - 0x6Bff (13 sectors = 208 pages)
 * data 896 B		0x7C00 - 0x7F7F	(14 pages)
 * reserved 128 B 	0x7F80 - 0x7FFF (2 pages)
 */

#define IAP_LOCATION 					(0x0F001FF1)
#define IAP_COMMAND_READ_UID 			58U
#define IAP_COMMAND_PREPARE_SECTOR 		50U
#define IAP_COMMAND_WRITE				51U
#define IAP_COMMAND_ERASE_SECTOR 		52U
#define IAP_COMMAND_BLANK_CHECK_SECTOR	53U
#define IAP_COMMAND_COMPARE				56U
#define IAP_COMMAND_ERASE_PAGE			59U
#define IAP_COMMAND_EXTENDED_READ_SIGN	73U
#define IAP_COMMAND_READ_EEPROM_PAGE    80U

#define FLASH_PAGE_SIZE       			(64)
#define FLASH_PAGE_COUNT      			(512)
#define FLASH_SECTOR_SIZE         		(1024)
#define FLASH_CCLK						(1200)





typedef enum {
	feSuccess = 0U,
	feStoreLengthInvalid,
	feErasePageFailed,
    feWritePageFailed,
    feBlankCheckFailed,
    feCompareFailed
} FlashError_t;

uint8_t Flash_EraseSector(uint32_t addr);
uint8_t Flash_BlankCheckSector(uint32_t startSector, uint32_t endSector);
uint8_t Flash_Compare(uint32_t dstAddr, uint32_t * srcAddr, uint16_t numOfBytes);
uint8_t Flash_Write(uint32_t addr, const uint8_t *data, const uint16_t length);
uint8_t Flash_ReadSignature(uint32_t startPage, uint32_t endPage, uint32_t numOfState, uint32_t *signature);
uint8_t Flash_GetUid(uint32_t * uid);
uint8_t Flash_StoreData(uint32_t addr, const uint8_t * data, const uint16_t length);
uint8_t Flash_ReadData(uint32_t addr, uint8_t * data, uint16_t length);
uint32_t * Flash_PointData(uint32_t addr);

/*


#define DATA_STORE_HEADER_SIZE          4U
#define DATA_STORE_MAX_DATA_SIZE        56U
#define DATA_STORE_MAX_SIZE             64U
#define DATA_STORE_HEADER               0xcafe

typedef enum {
    storeSuccess = 0U,
    storeErrDataLength,
    storeErrNoHeader,
    storeErrCrc
} DataStoreError_t;

typedef struct {
    uint32_t header;
    uint8_t data[DATA_STORE_MAX_DATA_SIZE];
    uint32_t crc;
} DataStore_t;

uint8_t Store_Save(uint32_t addr, uint8_t data, size_t length){

    DataStore_t storeData;
    uint32_t crc;

    if (length > DATA_STORE_MAX_DATA_SIZE){
        return storeErrDataLength;
    }

    storeData.header = DATA_STORE_HEADER;
    memset(storeData.data, 0, DATA_STORE_MAX_DATA_SIZE);
    memcpy(storeData.data, data, length);
    crc = crc32((void *)&storeData, (uint32_t)(DATA_STORE_HEADER_SIZE+DATA_STORE_MAX_DATA_SIZE));
    storeData.crc = crc;

    Flash_SaveData(addr, (uint8_t *)&storaData, DATA_STORE_MAX_SIZE);

    return storeSuccess;
}

uint8_t Store_Retrieve(uint32_t addr, uint8_t data, size_t length){

    DataStore_t storeData;
    uint32_t crc;

    if (length > DATA_STORE_MAX_DATA_SIZE){
        return storeErrDataLength;
    }

    Flash_ReadData(addr, (uint8_t *)&storaData, DATA_STORE_MAX_SIZE);

    if (storeData.header != DATA_STORE_HEADER{
        return storeErrNoHeader;
    }

    crc = crc32((void *)&storeData, (uint32_t)(DATA_STORE_HEADER_SIZE+DATA_STORE_MAX_DATA_SIZE));
    if (storeData.crc != crc){
        return storeErrCrc;
    }

    memcpy(data, storeData.data, length);

    return storeSuccess;

}

 */

#endif /* Flash_H_ */
